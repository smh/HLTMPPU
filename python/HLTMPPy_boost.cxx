#include <cstdio>
#include <memory>
#include <sstream>

#include <string>

#include <boost/dll.hpp>
#include <boost/python.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include "hltinterface/HLTInterface.h"

#include "HLTMPPU/HLTMPPU.h"
#include "HLTMPPU/DcmEmulator/DcmEmulator.h"
#include "HLTMPPU/HLTMPPU_utils.h"

using creator = hltinterface::HLTInterface*();
using destroyer = void (hltinterface::HLTInterface*);

boost::property_tree::ptree str2ptree(std::string ptreestr) {
  std::stringstream ss(ptreestr);
  boost::property_tree::ptree pt;
  int fl =  boost::property_tree::xml_parser::no_comments|
    boost::property_tree::xml_parser::trim_whitespace;
  boost::property_tree::xml_parser::read_xml(ss, pt, fl);
  return pt;
}

// Boost python bindings allow converting C++ classes into python classes with minimal typing.
// The C++ wrapper classes defined here are needed as boost::property_tree::ptree isn't available in
// python, and we need to use std::string arguments instead of ptree.

//! Wrapper class for DcmEmulator
class DcmEmulatorWrapper {
 private:
  std::shared_ptr<HLTMP::DcmEmulator> m_dcm;

 public:
  DcmEmulatorWrapper(std::string ptreestr) {
    auto pt = str2ptree(ptreestr);
    m_dcm = std::make_shared<HLTMP::DcmEmulator>(pt);
  }

  //! Wrapper to call any state transtion function from DcmEmulator
  //! \param method function reference to the DcmEmulator method
  //! \param dcmemu DcmEmulator instance
  void callMethod(std::function<void(HLTMP::DcmEmulator&)> method, HLTMP::DcmEmulator& dcmemu) {
    try {
      method(dcmemu);
    } catch(const ers::Issue& ex) {
      ers::warning(ex);
      std::cerr << "DcmEmulatorWrapper caught ERS exception during state transition : " << ex.what() << std::endl;
      exit(1);
    } catch(const std::exception &ex) {
      std::cerr << "DcmEmulatorWrapper caught exception during state transition : " << ex.what() << std::endl;
      exit(1);
    }
  }

  void configure() {
    callMethod(std::mem_fn(&HLTMP::DcmEmulator::configure), *m_dcm.get());
  }

  void connect() {
     callMethod(std::mem_fn(&HLTMP::DcmEmulator::connect), *m_dcm.get());
  }

  //! prepareForRun is special as it receives the RunParameters
  void prepareForRun(std::string ptreestr) {
    auto pt = str2ptree(ptreestr);
    m_dcm->prepareForRun(pt);
  }

  void stopRun() {
     callMethod(std::mem_fn(&HLTMP::DcmEmulator::stopRun), *m_dcm.get());
  }

  void disconnect() {
     callMethod(std::mem_fn(&HLTMP::DcmEmulator::disconnect), *m_dcm.get());
  }

  void unconfigure() {
     callMethod(std::mem_fn(&HLTMP::DcmEmulator::unconfigure), *m_dcm.get());
  }

  ~DcmEmulatorWrapper() {}
};

//! Wrapper class for HLTMPPU
class HLTMPPUWrapper {
 private:
  std::shared_ptr<HLTMPPU> m_pu;

 public:
  HLTMPPUWrapper() {
    m_pu = std::make_shared<HLTMPPU>();
    m_state = "LOADED";
  }

  //! Wrapper to call any state transtion function from HLTInterface, as they all have the same signature
  //! \param method function reference to the HLTInterface method
  //! \param mppu HLTMPPU instance
  //! \param ptreestr serialized ptree as xml string
  //! \param next_state next state
  //! \return true: success
  bool callMethod(std::function<bool(HLTMPPU&, const boost::property_tree::ptree&)> method,
                  HLTMPPU& mppu,
                  std::string ptreestr,
                  std::string next_state) {
    bool ret;
    try {
      auto pt = str2ptree(ptreestr);
      ret = method(mppu, pt);
    } catch(const ers::Issue& ex) {
      ers::warning(ex);
      std::cerr << "HLTMPPUWrapper caught ERS exception during state transition : " << ex.what() << std::endl;
      std::cerr << "- Current state : "  << m_state << std::endl;
      std::cerr << "- Aimed state   : "  << next_state << std::endl;
      exit(1);
    } catch(const std::exception &ex) {
      std::cerr << "HLTMPPUWrapper caught exception during state transition : "  << ex.what() << std::endl;
      std::cerr << "- Current state : "  << m_state << std::endl;
      std::cerr << "- Aimed state   : "  << next_state << std::endl;
      exit(1);
    }
    m_state = next_state;
    return ret;
  }

  bool configure(std::string ptreestr) {
    return callMethod(std::mem_fn(&HLTMPPU::configure), *m_pu.get(), ptreestr, "CONFIGURED");
  }

  bool connect() {
    return callMethod(std::mem_fn(&HLTMPPU::connect), *m_pu.get(), "", "CONNECTED");
  }

  //! prepareForRun is special, as forking happens here
  bool prepareForRun(std::string ptreestr) {
    auto currpid = getpid();

    // prepareForRun will fork children
    auto ret = callMethod(std::mem_fn(&HLTMPPU::prepareForRun), *m_pu.get(), ptreestr, "PREPARED");

    auto newpid = getpid();

    if (currpid != newpid) {  // Running in child
      if (ret) {  // Return code=1 means succesful execution, exit normally
        m_pu.reset();
        exit(0);
      } else {
        exit(1);  // Exit with non-0 code, this will be caught by the mother
      }
    }
    // Parent
    return ret;
  }

  bool disconnect() {
    return callMethod(std::mem_fn(&HLTMPPU::disconnect), *m_pu.get(), "", "CONFIGURED");
  }

  bool stopRun() {
    return callMethod(std::mem_fn(&HLTMPPU::stopRun), *m_pu.get(), "", "CONNECTED");
  }

  bool unconfigure() {
    return callMethod(std::mem_fn(&HLTMPPU::unconfigure), *m_pu.get(), "", "LOADED");
  }

  bool UserCommand(std::string ptreestr) {
    return callMethod(std::mem_fn(&HLTMPPU::hltUserCommand), *m_pu.get(), ptreestr, m_state);  // Stay in current state
  }

  unsigned numberOfActiveChildren() {
    return m_pu->numberOfActiveChildren();
  }

  std::string m_state;

  ~HLTMPPUWrapper() {}
};

//! Wrapper class that can load any HLTInterface implementation.
//! This is a more generalized version of HLTMPPUWrapper
class HLTInterfaceWrapper {
 private:
  std::shared_ptr<hltinterface::HLTInterface> m_pu;
  boost::dll::shared_library m_hltlib;

 public:
  //! Constructor, initializes HLTInterface instance using the library name
  //! \param hltdll Name of the library that has HLTInterface implementation, create_interface and
  //!               destroy_interface methods
  explicit HLTInterfaceWrapper(std::string hltdll = "libHLTMPPU.so") {
    try {
      // Loaded library is supposed to have an HLTInterface implementation,
      // create_interface and destroy_interface methods
      m_hltlib = boost::dll::shared_library(hltdll,
                                      boost::dll::load_mode::type::search_system_folders |
                                      boost::dll::load_mode::type::rtld_global);
    } catch (boost::system::system_error & ex) {
      std::cerr << "Can't open HLTMPPU dll " << hltdll << " due to : " << ex.what() << "\n";
      exit(1);
    }

    std::function<creator> c;
    std::function<destroyer> d;
    try {
      c = m_hltlib.get<creator>("create_interface");
      d = m_hltlib.get<destroyer>("destroy_interface");
    } catch  (boost::system::system_error & ex) {
      std::cerr << "Error loading create_interface/destroy_interface due to : " << " - " << ex.what() << "\n";
      exit(1);
    }

    auto hltmppu = c();
    m_pu.reset(hltmppu, d);

    m_state = "LOADED";
  }

  //! Wrapper to call any state transtion function from HLTInterface, as they all have the same signature
  //! \param method function reference to the HLTInterface method
  //! \param HLTInterface instance
  //! \param serialized ptree as xml string
  //! \param next state
  //! \return true: success
  bool callMethod(std::function<bool(hltinterface::HLTInterface&, const boost::property_tree::ptree&)> method,
                  hltinterface::HLTInterface& hltint,
                  std::string ptreestr,
                  std::string next_state) {
    bool ret;
    try {
      auto pt = str2ptree(ptreestr);
      ret = method(hltint, pt);
    } catch(const ers::Issue& ex) {
      ers::warning(ex);
      std::cerr << "HLTInterfaceWrapper caught ERS exception during state transition : " << ex.what() << std::endl;
      std::cerr << "- Current state : "  << m_state << std::endl;
      std::cerr << "- Aimed state   : "  << next_state << std::endl;
      exit(1);
    } catch(const std::exception &ex) {
      std::cerr << "HLTInterfaceWrapper caught exception during state transition : " << ex.what() << std::endl;
      std::cerr << "- Current state : "  << m_state << std::endl;
      std::cerr << "- Aimed state   : "  << next_state << std::endl;
      exit(1);
    }
    m_state = next_state;
    return ret;
  }

  bool configure(std::string ptreestr) {
    return callMethod(std::mem_fn(&hltinterface::HLTInterface::configure), *m_pu.get(), ptreestr, "CONFIGURED");
  }

  bool connect() {
    return callMethod(std::mem_fn(&hltinterface::HLTInterface::connect), *m_pu.get(), "", "CONNECTED");
  }

  //! prepareForRun is special, as forking happens here
  bool prepareForRun(std::string ptreestr) {
    auto currpid = getpid();

    // prepareForRun will fork children
    auto ret = callMethod(std::mem_fn(&hltinterface::HLTInterface::prepareForRun), *m_pu.get(), ptreestr, "PREPARED");

    auto newpid = getpid();

    if (currpid != newpid) {  // Running in child
      if (ret) {  // Return code=1 means succesful execution, exit normally
        m_pu.reset();
        exit(0);
      } else {
        exit(1);  // Exit with non-0 code, this will be caught by the mother
      }
    }
    // Parent
    return ret;
  }

  bool stopRun() {
    return callMethod(std::mem_fn(&hltinterface::HLTInterface::stopRun), *m_pu.get(), "", "CONNECTED");
  }

  bool disconnect() {
    return callMethod(std::mem_fn(&hltinterface::HLTInterface::disconnect), *m_pu.get(), "", "CONFIGURED");
  }

  bool unconfigure() {
    return callMethod(std::mem_fn(&hltinterface::HLTInterface::unconfigure), *m_pu.get(), "", "LOADED");
  }

  bool UserCommand(std::string ptreestr) {
    return callMethod(std::mem_fn(&hltinterface::HLTInterface::hltUserCommand), *m_pu.get(), ptreestr, m_state);  // Stay in current state
  }

  //! Current state of MPPU
  std::string m_state;

  ~HLTInterfaceWrapper() {}
};

using namespace boost::python;
// This is the part that converts C++ classes to python
BOOST_PYTHON_MODULE(libHLTMPPy_boost)
{
  class_<HLTMPPUWrapper>("HLTMPPUPy")
    .def("Configure", &HLTMPPUWrapper::configure)
    .def("Connect", &HLTMPPUWrapper::connect)
    .def("Disconnect", &HLTMPPUWrapper::disconnect)
    .def("PrepareForRun", &HLTMPPUWrapper::prepareForRun)
    .def("StopRun", &HLTMPPUWrapper::stopRun)
    .def("Unconfigure", &HLTMPPUWrapper::unconfigure)
    .def("UserCommand", &HLTMPPUWrapper::UserCommand)
    .def("NumberOfActiveChildren", &HLTMPPUWrapper::numberOfActiveChildren)
    .def_readonly("state", &HLTMPPUWrapper::m_state)
  ;
  class_<HLTInterfaceWrapper>("HLTMPPUPy_Dynamic")
    .def(init<std::string>())
    .def("Configure", &HLTInterfaceWrapper::configure)
    .def("Connect", &HLTInterfaceWrapper::connect)
    .def("Disconnect", &HLTInterfaceWrapper::disconnect)
    .def("PrepareForRun", &HLTInterfaceWrapper::prepareForRun)
    .def("StopRun", &HLTInterfaceWrapper::stopRun)
    .def("Unconfigure", &HLTInterfaceWrapper::unconfigure)
    .def("UserCommand", &HLTInterfaceWrapper::UserCommand)
    .def_readonly("state", &HLTInterfaceWrapper::m_state)
  ;
  class_<DcmEmulatorWrapper>("DcmEmulatorPy", init<std::string>())
    .def("Configure", &DcmEmulatorWrapper::configure)
    .def("Connect", &DcmEmulatorWrapper::connect)
    .def("PrepareForRun", &DcmEmulatorWrapper::prepareForRun)
    .def("StopRun", &DcmEmulatorWrapper::stopRun)
    .def("Disconnect", &DcmEmulatorWrapper::disconnect)
    .def("Unconfigure", &DcmEmulatorWrapper::unconfigure)
  ;
}
