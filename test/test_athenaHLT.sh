#!/usr/bin/env bash
# Test athenaHLT with current build of HLTMPPU.
# This is run in the CI but can also be run from your local build area.

if [ -z "${CONFIG}" ]; then
    export CONFIG=${CMTCONFIG}
fi

if [ -z "${CONFIG}" ]; then
    echo "Cannot deduce platform. Set \$CONFIG or \$CMTCONFIG."
    exit 1
fi

# Setup athena environment:
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
lsetup "asetup Athena,master,latest --platform=${CONFIG}"

# Exit for any failure:
set -e

# This avoids that setup.sh sets up the full tdaq release:
export TDAQ_INST_PATH=${TDAQ_RELEASE_BASE}

# Prepend HLTMPPU build to environment:
source installed/setup.sh

# Run the test:
mkdir -p test_athenaHLT
cd test_athenaHLT
test_trigP1_CalibPeb_concurrent_build.py
