// Dear emacs, this is -*- c++ -*-
#ifndef DF_FILE_EVENT_H
#define DF_FILE_EVENT_H

#include <mutex>
#include "dfinterface/dfinterface.h"
#include "eformat/ROBFragment.h"

namespace eformat{
  namespace read{
    class FullEventFragment;
  }
}

namespace HLTMP{

  /*! \brief Provides access to the event data.
   *
   *  \remark \ref Event pointers are obtained from a \ref Session instance and they must be returned
   *  to that instance when the processing is complete.
   */
  class DFFileEvent:public daq::dfinterface::Event
  {
  public:
    DFFileEvent(std::unique_ptr<const uint32_t[]> b,uint64_t gid=0);
    /*! \brief Returns the Level-1 ID of the event.
     *  \note  The interface implementation has to keep track of the Level-1 ID
     */
    virtual uint32_t l1Id() override;

    virtual void getL1Result(std::unique_ptr<uint32_t[]>& l1Result) override ;

    /*! \brief Informs the dfinterface implementation that the specified ROBs are likely to be requested in
     *      the future.
     *
     *  \param[in] robIds IDs of the requested ROBs.
     *
     *  \exception boost::system::system_error is thrown in case of network communication issues 
     */
    virtual void mayGetRobs(const std::vector<uint32_t>& robIds) override;

    /*! \brief Retrieves the specified ROB fragments.
     *
     *  \param[in] robIds IDs of the requested ROBs.
     *  \param[out] robs The requested ROBFragment objects are pushed back into this vector.
     *
     *  \exception boost::system::system_error is thrown in case of network communication issues 
     *
     *  The ROBFragment objects in \c robs are valid for the entire lifetime of this \ref Event
     *  object. If a fragment cannot be retrieved due to errors, a special empty fragment will be
     *  provided instead.
     */
    virtual void getRobs(const std::vector<uint32_t>& robIds,
			 std::vector<hltinterface::DCM_ROBInfo>& robs) override;

    /*! \brief Retrieves all the available ROB fragments.
     *
     *  \param[out] robs The requested ROBFragment objects are pushed back into this vector.
     *
     *  \exception boost::system::system_error is thrown in case of network communication issues 
     *
     *  The ROBFragment objects in \c robs are valid for the entire lifetime of this \ref Event
     *  object. If a fragment cannot be retrieved due to errors, a special empty fragment will be
     *  provided instead. Fragments already retrieved with get_robs() are nevertheless included in
     *  \c robs.
     */
    virtual void getAllRobs(std::vector<hltinterface::DCM_ROBInfo>& robs) override ;

    /*! \brief Returns global event id.
     */
    virtual uint64_t gid() override;

    /*! \brief Returns lumiblock of event.
     */
    virtual uint16_t lumiBlock() override;
    
    /*! \brief D'tor
     */
    virtual ~DFFileEvent();

    std::shared_ptr<const eformat::read::FullEventFragment> getCurrentEvent() const { return m_currEvent;}

    void setExtraL1Robs(std::vector<uint32_t> robs) { m_extraL1Robs = robs;}

  private:
    std::set<uint32_t> m_collectedRobs;
    std::shared_ptr<const eformat::read::FullEventFragment> m_currEvent;
    std::map<uint32_t, const uint32_t*> m_IDmap; ///< The ID <-> ROB map    
    std::vector<eformat::ROBFragment<const uint32_t*> > m_l1r; ///< The LVL1 result ROBs retrieved from the file
    std::vector<eformat::write::ROBFragment> m_l1robs; // l1robs to be written to new event
    std::unique_ptr<const uint32_t[]> m_blob;
    uint64_t m_gid;
    uint64_t m_lb;
    std::vector<uint32_t> m_extraL1Robs;  // List of extra ROBs that should be retrieved in L1 results
    std::mutex m_mutex;  // Mutex to protects concurrent getRobs calls
  };
}

#endif
