// Dear emacs, this is -*- c++ -*-
#ifndef __HLTMPPU_H
#define __HLTMPPU_H

#include <string>
#include <set>
#include <sys/types.h>
#include <unistd.h>
#include <deque>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <memory>
#include <chrono>
#include <future>
#include <atomic>

#include "hltinterface/HLTInterface.h"
#include "boost/thread/thread.hpp"
#include <boost/chrono/time_point.hpp>
#include <boost/chrono/system_clocks.hpp>
#include <boost/dll.hpp>

namespace HLTMP{
  class DataSource;
  class InfoService;
  class HLTMPPUInfo;
  class HLTMPPUMotherInfo;
}

namespace hltinterface{
  class IInfoRegister;
}

class TH1F;
class ISInfoDictionary;

class HLTMPPU : public hltinterface::HLTInterface {
public:
  HLTMPPU(); 
  ~HLTMPPU();
  bool configure(const boost::property_tree::ptree& args) override;
  bool connect(const boost::property_tree::ptree& args) override;
  bool prepareForRun(const boost::property_tree::ptree& args)override ;
  bool stopRun(const boost::property_tree::ptree& args) override;
  bool disconnect(const boost::property_tree::ptree& args)override;
  bool unconfigure(const boost::property_tree::ptree& args)override;
  bool publishStatistics(const boost::property_tree::ptree& args)override;
  bool hltUserCommand(const boost::property_tree::ptree& args)override;
  bool doEventLoop() override;
  bool prepareWorker(const boost::property_tree::ptree& args)override;
  bool finalizeWorker(const boost::property_tree::ptree& args)override;

  unsigned numberOfActiveChildren();
private:
  pid_t forkChildren(int pos);
  bool doProcessLoop(const boost::property_tree::ptree& args,int childNo);
  void doNannyWork();
  void collectChildExitStatus();
  void terminateChildren(int timeOut);
  void printPtree(const boost::property_tree::ptree& args,std::string level);
  void startNanny();
  void stopNanny();

  //! Get(From DataSource) and calculate event processing statistics
  void getAndCalculateChildStatistics();

  //! Print cumulated statistics for event processing
  void printChildStatistics();
  void statsPublisher();
  void startMotherPublisher();
  void stopMotherPublisher();
  void doMotherPublication();
  void publishMotherInfo(std::shared_ptr<ISInfoDictionary> dict,const std::string & name);
  void printOpenFDs(const std::string&);
  void printTasks(const std::string&);
  unsigned int countThreads();  //! Count number of extra threads (exclude the main thread)
  void runTimer();
  void waitForFreeMem(int maxSleep=100);

  //! Try to set pgid of child, update m_myChildren, m_posPidMap
  //! \param pid pid of mother process
  //! \param pos Slot where fork is started in
  void motherPostForkActions(pid_t pid, int pos);

  //! Create app name of child in mother process using
  //! TDAQ_APPLICATION_NAME environment variable
  //! and position/slot of the child process
  std::string getAppName(int position);

  //! Find matching child name that is part of input string
  //!
  //! Check if the input name contains any of the keys of m_childPidMap. If none of the child
  //! names match the input string, return input string
  //! (Example: if there is a child with name HLTMPPU-01, both of following inputs will match
  //! this child name: HLTMPPU-01 or HLTMPPU-01-04)
  //!
  //! \param input Input string, this can be either session name or child name
  //! \return child name or input string in case of no match
  std::string findMatchingChildName(std::string input);

  //! Update relevant IS information and containers when a child exits
  //! \param pid pid of child
  void updateExitedChildren(pid_t pid);

  // boost::dll::shared library objects should be declared before the plugins
  // such as m_HLTSteering, m_dataSource, m_infoService
  boost::dll::shared_library m_datasource_library;
  boost::dll::shared_library m_infoservice_library;
  std::vector<boost::dll::shared_library> m_hlt_libraries;
  pid_t m_myPid;
  pid_t m_myPgid;
  std::map<pid_t,int> m_myChildren,m_exitedChildren;
  std::map<int,pid_t> m_posPidMap;
  std::map<std::string,pid_t> m_childPidMap;
  std::deque<int> m_availableSlots;
  bool m_processEvents,m_terminationStarted;
  int m_numChildren;
  int m_FinalizeTimeout;
  boost::thread m_nannyThread,m_publisherThread,m_motherPublisher;
  std::atomic<bool> m_nannyWork,m_publisherWork;
  hltinterface::HLTInterface  *m_HLTSteering;
  HLTMP::DataSource *m_dataSource;
  //HLTMP::InfoService *m_infoService;
  hltinterface::IInfoRegister *m_infoService;
  std::string m_myName;
  std::string m_ISSName;
  std::string m_childLogPath;
  std::string m_childLogName;
  boost::chrono::steady_clock::time_point m_lastPublish;
  std::chrono::steady_clock::time_point m_TOTimerStart;
  unsigned int m_publishInterval;  // Interval of publication for HLTMPPU monitoring information such as histograms and IS
  unsigned int m_forkDelay;  // Upper limit of random amount of time to sleep between forks in milliseconds
  unsigned int m_preforkSleep;  // Amount of time in milliseconds to sleep before start forking
  int m_termStagger;  // Maximum seconds to wait for enough memory to become available for unsharing
  bool m_dumpFD;  // dump open file descriptors before and after forking
  bool m_dumpThreads;  // dump active threads before and after forking
  bool m_saveConfigOpts;  // Save boost::property_tree to files at each FSM state
  bool m_keepNumForks;  // Reset the number of children back to number of forks at the end of stopRun()
  bool m_skipFinalizeWorker;  // Skip finalize in children (to avoid memory unsharing)
  bool m_exitImmediately;  // Exit from the program through std::exit at the end of event loop.
  std::shared_ptr<HLTMP::HLTMPPUMotherInfo> m_motherInfo;
  std::unique_ptr<HLTMP::HLTMPPUInfo> m_childInfo;
  boost::property_tree::ptree m_configTree,m_prepareForRunTree;
  std::mutex m_statMutex;
  std::mutex m_nannyMutex;
  int m_myPos;
  bool m_dontPublishIS;  //! Variable used only for offline(runHLTMPPy,athenaHLT)
                         //! to avoid warning during IS publication. In the online,
                         //! this is by default set to false

  unsigned int m_allowedExtraThreadsAtFork;  //! Before forking, HLTMPPU will wait for m_threadWaitTimeout seconds
                                    //! until countThreads() return this number or less

  unsigned int m_threadWaitTimeout;  //! Seconds to wait for threads to close before forking. If number of
                                     //! extra threads doesn't become m_allowedExtraThreadsAtFork or lower
                                     //! at timeout, print a warning message
};

#endif
