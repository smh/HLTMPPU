
#ifndef HLTMPPU_DCM_EMULATOR_SHARED_DATA_H_
#define HLTMPPU_DCM_EMULATOR_SHARED_DATA_H_

#include <string>
#include <sstream>


#include <boost/interprocess/sync/interprocess_mutex.hpp>
#include <boost/interprocess/sync/interprocess_condition.hpp>

//! Object to hold information for a single session
struct session_info {
  enum { LineSize = 1000 };

  session_info() :dcm_running(false) {}

  bool dcm_running;

  //! Mutex to protect access to the queue
  boost::interprocess::interprocess_mutex      mutex;

  //! Condition to wait for next event
  boost::interprocess::interprocess_condition  cond_next;

  boost::interprocess::interprocess_condition  cond_ready;

  //! Name of the shared memory object where event data will be stored
  //! This should be a unique name containing session_name-global_id
  char shared_name[LineSize];

  // global ID of event
  uint64_t globalID;

  // True if no more events
  bool nomoreevents;

  void print() {
    std::cout << tostring();
  }

  std::string tostring() {
    std::ostringstream oss;
    oss << "Session Shared Memory:  " << "\n"
        << "  dcm_running  : " << dcm_running << "\n"
        << "  shared_name  : " << shared_name << "\n"
        << "  globalID     : " << globalID << "\n"
        << "  nomoreevents : " << nomoreevents << "\n";
    return oss.str();
  }
};

//! Object to hold information for a single event
struct event_info {
  event_info(): accept(false) {}

  //! Final decision to accept/reject event
  bool accept;

  //! Mutex to protect access to the queue
  boost::interprocess::interprocess_mutex      mutex;

  //! Condition to wait for hlt_results
  boost::interprocess::interprocess_condition  cond_hltresult;

  //! ROS Statistics in form of a json tree
  char ros_stats[100000];

  void print() {
    std::cout << tostring();
  }

  std::string tostring() {
    std::ostringstream oss;
    oss << "Event Shared Memory:" <<"\n"
        << "  accept?          : " << accept << "\n";

    return oss.str();
  }
};

#endif  // HLTMPPU_DCM_EMULATOR_SHARED_DATA_H_

