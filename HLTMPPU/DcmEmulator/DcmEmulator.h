#ifndef HLTMPPU_DCM_EMULATOR_H_
#define HLTMPPU_DCM_EMULATOR_H_

#include <map>
#include <string>
#include <mutex>
#include <atomic>
#include <thread>
#include <vector>
#include <iostream>
#include <future>

#include "ers/ers.h"

#include "eformat/compression.h"

#include "HLTMPPU/DcmEmulator/dcm_emulator_shared_data.h"
#include "HLTMPPU/DcmEmulator/SharedMemoryUtils.h"
#include "HLTMPPU/DcmEmulator/FileReaderWriter.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/interprocess/sync/interprocess_mutex.hpp>
#include <boost/interprocess/sync/interprocess_condition.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>

using namespace boost::interprocess;

namespace eformat {
  enum Compression;
}

class DcmEmulator;

namespace HLTMP {
//! Class that manages shared memory of an arbitrary name and size
class SharedMemoryHandler {
 public:
  SharedMemoryHandler(const std::string name, const size_t mem_size);
  ~SharedMemoryHandler();

  void * getAddress() { return m_region.get_address();}

 private:
  //! Name of the shared memory object
  std::string m_name;

  //! Mapped region for shared memory
  mapped_region m_region;

  //! shared_memory_object for the shared memory
  shared_memory_object m_shared_memory;
};

//! Class that represents the handler for each dfinterface::Session
class DcmSessionHandler {
 private:
  std::string m_name;

  //! Pointer to shared memory area for the session
  session_info * m_session_info;

  std::unique_ptr<SharedMemoryHandler> m_smh;

 public:
  friend class DcmEmulator;  // DcmEmulator can access private members

  //! \param name Name of the session, used to access shared memory
  explicit DcmSessionHandler(const std::string name);
  ~DcmSessionHandler() {
    ERS_DEBUG(1, "Destroying DcmSessionHandler instance: " << m_name);
  }
};

//! Class that represents the handler for each event that is being processed
class DcmEventHandler {
 public:
  friend class DcmEmulator;  // DcmEmulator can access private members

  //! \param event Serialized read::FullEventFragment
  //! \param base_shared_name Base name for shared memory area
  //! \param max_fullev_size Maximum allowed size for the fullevent read from file
  //! \param max_hltres_size Maximum allowed size for the hltresult event that is sent back from HLT implementation
  DcmEventHandler(std::unique_ptr<uint32_t[]> event,
                  std::string base_shared_name,
                  size_t max_fullev_size,
                  size_t max_hltres_size);
  ~DcmEventHandler() {
    ERS_DEBUG(3, "Destroying DcmEventHandler instance: " << m_shared_name);
  }

 private:
  uint64_t m_gid;  //! Global ID
  std::string m_shared_name;  //! Name of the shared memory area

  //! Pointer to shared memory area for the event
  event_info * m_event_info;

  //! Shared vector handler for full event
  std::unique_ptr<SharedVectorHandler> m_fullevent;

  //! Shared vector handler for hlt result
  std::unique_ptr<SharedVectorHandler> m_hltresult;

  std::unique_ptr<SharedMemoryHandler> m_smh;
};

/*! Handles ROS statistics
 *
 *  Everytime there is a DataCollector::collect call from HLT implementation it
 *  translates to dfinterface::Session::getRobs or getAllRobs call.
 *  In online, these calls result in DCM communicating with corresponding ROS
 *  and requesting fragments. Purpose of this class is to generate statistics
 *  for this request to identify algorithms that cause excessive rates of
 *  requests on certain ROSes.
 *
*/
class ROSHitStats {
 private:
  // Statistic per ROS
  struct ROSFractions {
    double ros;  // Number of ros hits / number of events
    double rob;  // Number of roquested ROBs / number of events / Number of ROBs in ROS
    double size;  // Size of retrieved ROBs / number of events
  };

  //! ROS name-statistics for all requests
  std::map<std::string, ROSFractions> m_total;

  //! ROS name-statistics for calls to retrieve full event(getAllRobs)
  std::map<std::string, ROSFractions> m_evbuild;

  //! ROS name-statistics for calls to retrieve certain ROBs(getRobs)
  std::map<std::string, ROSFractions> m_noevbuild;

  //! ROS-Number of ROBs in each ROS
  std::map<std::string, uint32_t> m_nrobs;

  //! Number of events used in calculation, incremented by one at each "add()" call
  size_t m_nevents;

  //! Set to false if ROS2ROB map is empty
  bool m_enabled;

 public:
  //! \param config configuration tree with ROS2ROB information
  explicit ROSHitStats(const boost::property_tree::ptree & config);
  ~ROSHitStats() {}

  /*! add statistics of the event to m_total, m_evbuild, m_noevbuild
   *
   * \param event_stats Statistics of events serialized as a json in format:
   *    {
   *      "ROS1": {
   *        "noevbuild": {"ros": 12, "rob": 42, "size": 13123.23"},
   *        "evbuild": {"ros": 12, "rob": 42, "size": 13123.23"}
   *      },
   *      "ROS2": {
   *        "noevbuild": {"ros": 12, "rob": 42, "size": 13123.23"},
   *        "evbuild": {"ros": 12, "rob": 42, "size": 13123.23"}
   *      },
   *      ...
   *    }
   *
   */
  void add(std::string event_stats);

  /*! Calculate final statistics.
   *
   *  Calculate m_total from m_evbuild and m_noevbuild, average by
   *  the number of events and number of ROBs in each ROS
   *  \param nevents Number of total events
   */
  void calculate();

  //! Print all information as a table.
  //! \param out output stream to print to (default: stdout)
  void print(std::ostream & out = std::cout);

  //! Convert statistics to string
  std::string to_string();

  //! True is ROS2ROB element in config ptree is not empty
  bool isEnabled() {return m_enabled;}
};

/*! Class that emulates Dcm by
 *  - reading full events from file
 *  - writing an event to a file, changing it's HLT result ROBs and stream tags
 *  This class has to be initiated in a seperate application.
*/
class DcmEmulator {
 public:
  //! ctor initializes configuration parameters from ptree
  explicit DcmEmulator(const boost::property_tree::ptree & tree);

  ~DcmEmulator();

  //! Do nothing
  void configure();

  //! Do nothing
  void connect();

  //! Initialize m_sessions, start listener threads for each session
  //!
  //! The whole initialization is done in prepareForRun, because this method
  //! starts new threads, and this can only be done after HLTMPPy.prepareForRun
  //! in the python environment due to forking. Also we want stopRun and
  //! prepareForRun to restart everything including the file reader/writer
  void prepareForRun(const boost::property_tree::ptree & tree);

  //! stop listener threads, delete m_sessions, delete m_file_rw
  //! This method should only be called once all forks exited.
  void stopRun();

  //! Do nothing
  void disconnect();

  //! Do Nothing
  void unconfigure();

  bool noMoreEvents() {return m_nomoreevents;}

 private:
  //! Clean up m_event_futures for a session
  void cleanup_futures(std::string session_name);

  //! Start listening a certain session
  void listenSession(std::string session);

  //! Wait for event processing to complet for a certain global_ID.
  //! if event is accepted, create a FullEventFragment using the original
  //! event and hltresults, and write it to a file
  void waitForEventDone(const std::string& name);

  //! Get next event from FileReaderWriter and put serialized FullEventFragment in an array
  std::unique_ptr<uint32_t[]> getNextEvent();

  // key: name of shared memory area, value: DcmSessionHandler object that represent a session
  std::map<std::string, DcmSessionHandler> m_sessions;

  boost::property_tree::ptree m_args;  // Configuration ptree
  std::map<std::string, DcmEventHandler> m_event_handlers;  // key: session-gid, value: DcmEventHandler object
  std::mutex m_readmutex;  //! Protect reading of events from file
  std::mutex m_writemutex;  //! Protect writing of events to file
  std::mutex m_statmutex;  //! Protect ros hit statistic calculation
  std::mutex m_eventhandler_mutex;  //! Protect actions on m_event_handlers
  std::mutex m_eventfuture_mutex;  //! Protect actions on m_event_handlers
  std::string m_base_name;  // Base name for shared memory object (same as HLTMPPU application_name)
  uint32_t m_readEvents;  // Number of events read
  size_t m_nworkers;  // Maximum number of HLTMPPU workers
  size_t m_nevents_per_worker;  // Maximum number of events that can be processed in parallel
  bool m_stop;  //! True if stop signal received
  bool m_nomoreevents;  //! True if there are no more events in the file(s)

  size_t m_max_hltres_size;  //! Maximum HLT Result size in bytes, used for creating shared memory segments

  std::vector<std::thread> m_session_threads;  //! Threads that listen to each session for getNext calls

  //! list of futures for each session, where each future holds results of a waitForEventDone call
  std::map<std::string, std::list<std::future<void>>> m_event_futures;

  eformat::Compression m_comp;  //! Compression type of built event
  unsigned int m_compLevel;  //! Compression level of built event

  std::unique_ptr<FileReaderWriter> m_file_rw;  //! Takes care of reading events from file and writing them

  std::string m_ros_hit_stats_basename;  // Base name for ROS Hit Statistics output file
  ROSHitStats m_ros_hit_stats_accept;  // ROS Hit Statistics for accepted events
  ROSHitStats m_ros_hit_stats_reject;  // ROS Hit Statistics for rejected events
};
}  // namespace HLTMP
#endif  // HLTMPPU_DCMEMULATOR_H_

