// Dear emacs, this is -*- c++ -*-
#ifndef HLTMPPU_MONSVCINFOSERVICE_H
#define HLTMPPU_MONSVCINFOSERVICE_H
//#include "HLTMPPU/InfoService.h"
#include "hltinterface/IInfoRegister.h"
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <string>
#include <memory>
#include "monsvc/ptr.h"
#include <boost/regex.hpp>

namespace monsvc{
  class PublishingController;
  class ConfigurationRule;
}
namespace hltinterface{
  class GenericHLTContainer;
  class ContainerFactory;
}
class ISInfoDynAny;
class IPCPartition;
using ContPtr = std::shared_ptr<hltinterface::GenericHLTContainer>;
namespace HLTMP{
  class MonSvcInfoService:public hltinterface::IInfoRegister{
  public:
    ~MonSvcInfoService();
    MonSvcInfoService();
    //IInfoRegister stuff
    bool configure(const boost::property_tree::ptree &args) override;
    bool prepareForRun(const boost::property_tree::ptree &args) override;
    bool prepareWorker(const boost::property_tree::ptree &args) override;
    bool finalizeWorker(const boost::property_tree::ptree &args) override;
    bool finalize(const boost::property_tree::ptree &args) override;
    bool beginEvent(const boost::property_tree::ptree &) override;
    bool endEvent(const boost::property_tree::ptree &) override ;
    std::vector<std::shared_ptr<hltinterface::GenericHLTContainer> > queryISRegistry(const std::string& regex) override;
    bool registerObject(const std::string publishPath,
			ContPtr object) override;
    bool releaseObject(const std::string fullName);
    void get(const std::string& regex, THList& list) ;
    void getUnsummed(const std::string& regex, std::map<std::string, std::vector<TObject*> >& list) ;
    
    void clear(const std::string& regex ) ;
    
    void reset(const std::string& regex ) ;
    //sname= service name
    //id histogram id
    bool registerTObject (const std::string& sname, const std::string& id, 
    			  TObject* h) ;
    bool discoverTObject (const std::string& sname, const std::string& id,
    			  TObject *&h  ) ;

    bool releaseTObject (const std::string& sname, 
    			 const std::string& id ) ;

    void clearToRelease(const std::string& regex) ;

    hltinterface::IInfoRegister::mutex_type& getPublicationMutex() const; 
    static IInfoRegister* getInstance();
    void monsvcCallback(const std::string&,ISInfoDynAny*);
    void setModification(bool b);
    bool pullInfo(const std::string &publishPath,const std::string &objectName);
  private:
    class InfoHolder{
    public:
      //std::shared_ptr<hltinterface::GenericHLTContainer> cont;
      ContPtr cont;
      //std::shared_ptr<hltinterface::GenericHLTContainer> lastCopy;
      ContPtr lastCopy;
      ISInfoDynAny *isInfo;
      monsvc::ptr<ISInfoDynAny> monsvc_ptr;
      std::vector<size_t> intFieldMap;
      std::vector<size_t> floatFieldMap;
      std::vector<size_t> intVecFieldMap;
      std::vector<size_t> floatVecFieldMap;
      bool published;
    };
    using InfoPtr = std::shared_ptr<InfoHolder>;
    bool stripTag(const std::string &id,int &tag,std::string& path);
    void parseConfigurationBundle(const boost::property_tree::ptree &args);
    void parseConfigurationRule(const boost::property_tree::ptree &args);
    void renameProviders(const std::string &, boost::property_tree::ptree &args);
    virtual void findDiff(ContPtr last,ContPtr update);
    bool m_tempHist;
    int m_dbgLvl;
    std::shared_ptr<monsvc::PublishingController> m_publisher;
    std::unordered_set<std::string> *m_histoList,*m_tempList;
    std::unordered_map<std::string,std::unordered_set<std::string>* > *m_serviceHistoMap;
    std::vector<std::shared_ptr<monsvc::ConfigurationRule> > *m_configRules;
    boost::property_tree::ptree m_configTree;
    std::shared_ptr<hltinterface::ContainerFactory> m_factory;
    //for is objects
    std::unordered_map<std::string,std::shared_ptr<InfoHolder> > *m_objMap;
    std::vector<InfoPtr> *m_lbList;
    std::vector<InfoPtr> *m_perPublishList;
    IPCPartition* m_part;
    boost::regex m_lbRegex;
  };
}//end namespace

#endif
