#include "HLTMPPU/MonSvcInfoService.h"
#include "HLTMPPU/HLTMPPU_utils.h"
#include "monsvc/MonitoringService.h"
#include "monsvc/PublishingController.h"
#include "ipc/partition.h"
#include "TObject.h"
#include "TH3.h"
#include "TH2.h"
#include "TH1.h"
#include "TGraph2D.h"
#include "TGraph.h"
#include "ers/ers.h"
#include "oh/OHRootMutex.h"
#include <unordered_map>
#include <regex>
#include "hltinterface/GenericHLTContainer.h"
#include "hltinterface/ContainerFactory.h"
#include "is/infodynany.h"
#include <boost/algorithm/string.hpp>
#include "TThread.h" //Root stuff for thread safety

ERS_DECLARE_ISSUE(InfoSvc,                // namespace name
		  RegistryIssue,   // issue name
		  "Problem with object (un-)registration " << type << ".",        // message
		  ((const char *)type )             // first attribute
		  )

namespace HLTMP{
template<typename T> inline void setVector(std::vector<T> &out, const std::vector<double> &in){
    out.resize(in.size());
    for(size_t t=0;t<in.size();t++){
      out[t]=in[t];
    }
    return;
  }
  template<typename T> inline void setVector(std::vector<T> &out, const std::vector<long> &in){
    out.resize(in.size());
    for(size_t t=0;t<in.size();t++){
      out[t]=in[t];
    }
    return;
  }
}

HLTMP::MonSvcInfoService::~MonSvcInfoService(){
  delete m_histoList;
  for(auto it=m_serviceHistoMap->begin();it!=m_serviceHistoMap->end();++it){
    delete it->second;
  }
  delete m_serviceHistoMap;
  delete m_configRules;
  delete m_objMap;
  delete m_lbList;
  delete m_part;
  delete m_tempList;
  delete m_perPublishList;
}

HLTMP::MonSvcInfoService::MonSvcInfoService():m_lbRegex("^/?run_\\d+/lb_(\\d+)(/.+)$"){
  TH1::AddDirectory(false);
  ERS_LOG("Loading MonSvcInfoService (built on "<<__DATE__<<" "<<__TIME__ << ")");

  hltinterface::IInfoRegister::setInstance(this);
  ERS_DEBUG(1, "registered MonSvcInfoService as IInfoRegister");
  m_tempHist=false;
  m_histoList=new std::unordered_set<std::string>();
  m_tempList=new std::unordered_set<std::string>();
  m_serviceHistoMap=new std::unordered_map<std::string,std::unordered_set<std::string>*>();
  m_configRules=new std::vector<std::shared_ptr<monsvc::ConfigurationRule> >();
  m_objMap=new std::unordered_map<std::string,std::shared_ptr<InfoHolder> >();
  m_lbList=new std::vector<InfoPtr>();
  m_perPublishList=new std::vector<InfoPtr>();
  
  char *part=getenv("TDAQ_PARTITION");
  if(part){
    m_part=new IPCPartition(part);
  }else{
    m_part=0;
  }  
  try{
    m_factory=std::make_shared<hltinterface::ContainerFactory>();
    hltinterface::ContainerFactory::setInstance(m_factory);
  }catch(std::exception &ex){
    std::cerr<<HLTMP::getTimeTag()<<__PRETTY_FUNCTION__<<"It seems ContainerFactory instance is already initialized."<<std::endl
	     <<"      Caught exception "<<ex.what()<<std::endl;
    m_factory=hltinterface::ContainerFactory::getInstance();
  }
  m_dbgLvl=0;
}


bool HLTMP::MonSvcInfoService::configure(const boost::property_tree::ptree &args){
  
  m_configTree=args;
  try{
    auto extras=args.get_child("HLTMonInfoImpl.extraParams").equal_range("parameter");
    for(auto it=extras.first;it!=extras.second;++it){
      std::vector<std::string> tokens;
      std::string data=std::string(it->second.data());
      boost::algorithm::split(tokens,data,boost::is_any_of("="));
      if(tokens.size()>1){
	if(tokens.at(0)==std::string("debugLevel")){
	  int dbgLvl=::strtol(tokens.at(1).c_str(),0,10);
	  if(errno==ERANGE||errno==EINVAL){
	    ERS_LOG("extra parameter \"debugLevel\" is set incorrectly! offending line is \""<<data<<"\"");
	    errno=0;
	  }else{
	    m_dbgLvl=dbgLvl;
	  }
	}
      }
    }
  }catch(boost::property_tree::ptree_bad_path &ex){
    ERS_DEBUG(1,"there is no extraParams, skipping");
  }
  
  return true;
}

bool HLTMP::MonSvcInfoService::prepareForRun(const boost::property_tree::ptree& /*args*/){
  if(m_dbgLvl>0){
    ERS_LOG(" Total "<<m_histoList->size()<<"  histograms registered to keep");
  }
  if(m_dbgLvl>1){
    if(m_histoList->size())ERS_LOG("List of histograms currently registered");
    for(auto h:*m_histoList){
      std::cout<<"  "<<h<<std::endl;
    }
  }
  m_tempHist=true;
  return true;
}

void HLTMP::MonSvcInfoService::renameProviders(const std::string &provName, boost::property_tree::ptree& args){
  auto ruleBundles=args.equal_range("ConfigurationRuleBundle");
  if(m_dbgLvl>2)HLTMP::printPtree(args," InfoSvc renameProviders->");
  for(auto rb=ruleBundles.first;rb!=ruleBundles.second;++rb){
    auto ruleList=rb->second.get_child_optional("Rules");
    if(ruleList){
      auto rules=ruleList->equal_range("ConfigurationRule");
      for(auto r=rules.first;r!=rules.second;++r){
	auto provider=r->second.get_child_optional("Parameters.OHPublishingParameters.ROOTProvider");
	if(provider){
	  //ERS_LOG(provider->data());
	  provider->put_value(provName);
	  //ERS_LOG(provider->data());	  
	}
      }
    }
    auto linkedBundles=rb->second.get_child_optional("LinkedBundles");
    if(linkedBundles){
      renameProviders(provName,*linkedBundles);
    }
  }
  if(m_dbgLvl>2)HLTMP::printPtree(args," InfoSvc after renameProviders->");
}

bool HLTMP::MonSvcInfoService::prepareWorker(const boost::property_tree::ptree &args){
  TThread::Initialize();
  HLTMP::printPtree(args," InfoSvc prepareWorker->");
  std::string appName=std::string(args.get_child("appName").data());
  if(m_part){
    m_publisher=std::make_shared<monsvc::PublishingController>(*m_part,appName);
    try{
      auto confRules=m_configTree.get_child("HLTMonInfoImpl.ConfigurationRules");
      renameProviders(appName,confRules);
      if(m_dbgLvl>0){
	ERS_LOG("Using following tree to configure MonSvc");
	HLTMP::printPtree(confRules.get_child("ConfigurationRuleBundle"),HLTMP::getTimeTag()+" InfoSvc monsvcConfig->");
      }
      m_publisher->add_configuration_rules(confRules.get_child("ConfigurationRuleBundle"));
    }catch(boost::property_tree::ptree_bad_path &ex){
      std::cerr<<HLTMP::getTimeTag()<<"MonSvc configuration problem "<<ex.what()<<std::endl;
      return false;
    }catch(ers::Issue &e){
      ERS_LOG("Configuration of Monsvc failed "<<e.what());
      return false;
    }
    ERS_LOG("Starting publication ");
    m_publisher->start_publishing();
  }
  if(m_dbgLvl>1){
    if(m_histoList->size())ERS_LOG("List of histograms currently registered");
    for(auto h:*m_histoList){
      std::cout<<"  "<<h<<std::endl;
    }
  }
  return true;
}

bool HLTMP::MonSvcInfoService::finalizeWorker(const boost::property_tree::ptree& /* args */){
  m_publisher->stop_publishing();
  m_publisher->publish_all();
  m_publisher.reset();
  return true;
}

bool HLTMP::MonSvcInfoService::finalize(const boost::property_tree::ptree& /* args */){
  auto& minst=monsvc::MonitoringService::instance();
  size_t numHists=m_histoList->size();
  int tag=0;
  std::string path;
  for(auto h:*m_tempList){
    bool taggedObj=stripTag(h,tag,path);
    if(taggedObj){
      if(m_dbgLvl>2){
	ERS_LOG("Unregistering "<<h<<" with tag "<<tag);	
      }
      minst.remove_object(path,tag);
    }else{
      if(m_dbgLvl>2){
	ERS_LOG("Unregistering "<<h);	
      }
      minst.remove_object(h);
    }
    m_histoList->erase(h);
  }
  for(auto hl:*m_serviceHistoMap){
    for(auto h:*m_tempList)hl.second->erase(h);
  }
  //m_serviceHistoMap->clear();
  if(m_dbgLvl>0){
    std::cout<<"Removed "<<m_tempList->size()<<" of "<<numHists
	     <<" histograms. Remaining "<<m_histoList->size()
	     <<" histograms in Registry"<<std::endl;
  }
  m_tempList->clear();
  m_tempHist=false;

  if(m_dbgLvl>1){
    if(m_histoList->size())std::cout<<"Histograms remaining after Finalize()"<<std::endl; 
    for(auto h:*m_histoList){
      std::cout<<"  "<<h<<std::endl;
    }
  }
  return true;
}

void HLTMP::MonSvcInfoService::get(const std::string& regex, THList& list){
  std::regex pattern {regex};

  for (const auto & histname : *m_histoList) {
    if(regex_match(histname, pattern)) {
      int tag;
      std::string path;
      bool taggedObj=stripTag(histname, tag, path);
      TObject* t;
      if (taggedObj) {
        // Try all possible types
        try {
          auto ptr = monsvc::MonitoringService::instance().find_object<TObject>(path,tag);
          t = ptr.get();
        } catch (monsvc::WrongType & ex) {
          try {
            auto ptr = monsvc::MonitoringService::instance().find_object<TH1>(path,tag);
            t = dynamic_cast<TObject*>(ptr.get());
          } catch (monsvc::WrongType & ex) {
            auto ptr = monsvc::MonitoringService::instance().find_object<TGraph>(path,tag);
            t = dynamic_cast<TObject*>(ptr.get());
          }
        }
      } else {
        // Try all possible types
        try {
          auto ptr = monsvc::MonitoringService::instance().find_object<TObject>(histname);
          t = ptr.get();
        } catch (monsvc::WrongType & ex) {
          try {
            auto ptr = monsvc::MonitoringService::instance().find_object<TH1>(histname);
            t = dynamic_cast<TObject*>(ptr.get());
          } catch (monsvc::WrongType & ex) {
            auto ptr = monsvc::MonitoringService::instance().find_object<TGraph>(histname);
            t = dynamic_cast<TObject*>(ptr.get());
          }
        }
      }
      list.emplace(histname, t);
    }
  }
}

void HLTMP::MonSvcInfoService::getUnsummed(const std::string& regex, 
					   std::map<std::string, std::vector<TObject*> >& /*list*/){
  ERS_LOG("UNIMPLEMENTED "<<regex);
}

void HLTMP::MonSvcInfoService::clear(const std::string& regex){
  ERS_LOG("UNIMPLEMENTED "<<regex);
}

void HLTMP::MonSvcInfoService::reset(const std::string& regex){
  ERS_LOG("UNIMPLEMENTED "<<regex);
}

bool HLTMP::MonSvcInfoService::stripTag(const std::string &id,int &tag,std::string& path){
  boost::smatch what;
  if(m_dbgLvl>0)ERS_LOG("Checking for tagged histograms "<<id);
  if(boost::regex_match(id,what,m_lbRegex)){    
    if(m_dbgLvl>1)ERS_LOG("Tag Found tag="<<what[1]<<" at path="<<what[2]);
    try{
      tag=std::stoi(what[1]);
    }catch(std::exception &ex){
      ERS_LOG("Caught exception "<<ex.what()<<" when trying to parse tag value from "<<id);
      return false;
    }
    path=what[2];
    return true;
  }
  if(m_dbgLvl>1)ERS_LOG("Histogram "<<id<<" is not tagged");
  return false;
}

bool HLTMP::MonSvcInfoService::registerTObject(const std::string& sname, 
					       const std::string& id, 
					       TObject* ho){
  if(m_histoList->find(id)!=m_histoList->end()){
    ERS_LOG("Registering previously registered histogram \""<<id<<"\", registered for service "<<sname<<" ");
  }else{
    m_histoList->insert(id);
  }

  if(m_dbgLvl>2){
    ERS_LOG("Registering histogram \""<<id<<"\", for service \""<<sname<<"\"");
  }
  std::unordered_set<std::string>* hists=0;
  auto it=m_serviceHistoMap->find(sname);
  if(it==m_serviceHistoMap->end()){
    hists=new std::unordered_set<std::string>();
    m_serviceHistoMap->operator[](sname)=hists;
  }else{
    hists=it->second;
    if(hists->find(id)!=hists->end()){
      //ERS_LOG("Histogram "<<id<<" in service "<<sname<<" is already registered. NOT REGISTERING AGAIN!");
      ers::error(InfoSvc::RegistryIssue(ERS_HERE,("Histogram \""+id+ "\" in service "+sname+"is already registered ").c_str()));
      //return false;
    }
  }
  hists->insert(id);
  if(m_tempHist)m_tempList->insert(id);
  //need this to prevent Monsvc ignoring histograms
  TH1* h=0;
  TGraph *g=0;
  int tag;
  std::string path;
  bool taggedObj=stripTag(id,tag,path);
  h=dynamic_cast<TH1*>(ho);//having a TH1 is enough root provider figures out type itself
  if(h==0){
    g=dynamic_cast<TGraph*>(ho);
    if(g==0){
      TGraph2D *g2=dynamic_cast<TGraph2D*>(ho);
      if(g2==0){
	ERS_LOG("Type of the object "<<id<<" in service "<<sname
		<<" can't be determined! Will probably be ignored my MonSvc."
		<<std::endl<<"Registering in case MonSvc behavior is changed");
	if(taggedObj){
	  monsvc::MonitoringService::instance().register_object(path,tag,ho,true);
	}else{
	  monsvc::MonitoringService::instance().register_object(id,ho,true);
	}
      }  // TODO(cyildiz): Shouldn't we register it as TGraph2D here in else?
    }else{
      if(taggedObj){
	  monsvc::MonitoringService::instance().register_object(path,tag,g,true);
      }else{
	monsvc::MonitoringService::instance().register_object(id,g,true);
      }
    }
  }else{
    if(taggedObj){
      monsvc::MonitoringService::instance().register_object(path,tag,h,true);
    }else{
      monsvc::MonitoringService::instance().register_object(id,h,true);
    }
  }
  //m_registry->publish_object(id,h,0,0);

  return true;
  
}

bool HLTMP::MonSvcInfoService::discoverTObject(const std::string& sname, 
					       const std::string& id,
					       TObject*& h  ){
  try {
    auto hists = m_serviceHistoMap->at(sname);  // may throw out_of_range
    if (hists->find(id) == hists->end()) {
      h = 0;
      return false;
    }
    int tag;
    std::string path;
    bool taggedObj = stripTag(id, tag, path);

    if (taggedObj) {
      // Try all possible types
      try {
        auto ptr = monsvc::MonitoringService::instance().find_object<TObject>(path,tag);
        h = ptr.get();
      } catch (monsvc::WrongType & ex) {
        try {
          auto ptr = monsvc::MonitoringService::instance().find_object<TH1>(path,tag);
          h = dynamic_cast<TObject*>(ptr.get());
        } catch (monsvc::WrongType & ex) {
          auto ptr = monsvc::MonitoringService::instance().find_object<TGraph>(path,tag);
          h = dynamic_cast<TObject*>(ptr.get());
        }
      }
    } else {
      // Try all possible types
      try {
        auto ptr = monsvc::MonitoringService::instance().find_object<TObject>(id);
        h = ptr.get();
      } catch (monsvc::WrongType & ex) {
        try {
          auto ptr = monsvc::MonitoringService::instance().find_object<TH1>(id);
          h = dynamic_cast<TObject*>(ptr.get());
        } catch (monsvc::WrongType & ex) {
          auto ptr = monsvc::MonitoringService::instance().find_object<TGraph>(id);
          h = dynamic_cast<TObject*>(ptr.get());
        }
      }
    }
    return true;
  } catch (std::out_of_range & ex) {
    std::cerr << HLTMP::getTimeTag() << "Problem getting object with sname : "
              << sname << " from m_serviceHistoMap, error: " << ex.what() << "\n";
    h = 0;
    return false;
  } catch (std::exception & ex) {
    std::cerr << HLTMP::getTimeTag() << "Problem getting object with sname : "
              << sname << ", id: " << id << ". Error: " << ex.what();
    h = 0;
    return false;
  }
  return true;
}

bool HLTMP::MonSvcInfoService::releaseTObject(const std::string& sname, 
					      const std::string& id ){
  //ERS_LOG("UNIMPLEMENTED "<<sname<<" "<<id);
  if(m_dbgLvl>2){
    ERS_LOG("Un-Registering histogram \""<<id<<"\", for service \""<<sname<<"\"");
  }
  auto inList=m_histoList->find(id);
  if(inList==m_histoList->end()){
    ERS_LOG("Histogram \""<<id<<"\" for service "<<sname<<" was not registered before ");
    return false;
  }
  m_histoList->erase(inList);
  std::string path;
  int tag;
  bool taggedObj=stripTag(id,tag,path);
  if(taggedObj){
    monsvc::MonitoringService::instance().remove_object(path,tag);
  }else{
    monsvc::MonitoringService::instance().remove_object(id);
  }

  std::unordered_set<std::string>* hists=0;
  auto it=m_serviceHistoMap->find(sname);
  if(it==m_serviceHistoMap->end()){
    ERS_LOG("Service name "<<sname<<" is not known when requesting unregistering histogram"
	    <<id<<". Probably service name is given incorrectly");
    return false;
  }else{
    hists=it->second;
    if(hists->find(id)!=hists->end()){
      hists->erase(id);
    }
  }
  if(m_tempHist && (m_tempList->find(id)!=m_tempList->end()))m_tempList->erase(id);
  if(m_dbgLvl>1){
    ERS_LOG("Unregistered histogram "<<id<<" from sname="<<sname);
  }
  return true;
}

void HLTMP::MonSvcInfoService::clearToRelease(const std::string& regex){
  ERS_LOG("UNIMPLEMENTED "<<regex);
}

hltinterface::IInfoRegister::mutex_type& HLTMP::MonSvcInfoService::getPublicationMutex() const
{
  return OHRootMutex::getMutex();
}

void HLTMP::MonSvcInfoService::parseConfigurationBundle(const boost::property_tree::ptree &args){
  auto extras=args.get_child("Rules").equal_range("ConfigurationRule");
  for(auto it=extras.first;it!=extras.second;++it){
    parseConfigurationRule(it->second);
  }
  
}

void HLTMP::MonSvcInfoService::parseConfigurationRule(const boost::property_tree::ptree &args){
  std::cout<<"----------------------------------------"<<std::endl;
  HLTMP::printPtree(args," InfoSvc-> ");
  std::cout<<"----------------------------------------"<<std::endl;
  std::string incFilter=std::string(args.get_child("IncludeFilter").data());
  std::string excFilter=std::string(args.get_child("ExcludeFilter").data());
  std::string filtName=std::string(args.get_child("UID").data());
  //bool isISConfig=false;
  auto OHRule=args.get_child_optional("Parameters.OHPublishingParameters");
  if(OHRule){
    try{
      unsigned int interval=args.get("Parameters.OHPublishingParameters.PublishInterval",120);
      std::string serverName=std::string(args.get_child("Parameters.OHPublishingParameters.OHServer").data());
      std::string providerName=std::string(args.get_child("Parameters.OHPublishingParameters.ROOTProvider").data());
      char buff[2000];
      int len=snprintf(buff,2000,"%s:%s/%s=>oh:(%d,%s,%s)",
		       filtName.c_str(),
		       incFilter.c_str(),
		       excFilter.c_str(),
		       interval,
		       serverName.c_str(),
		       providerName.c_str()
		       );
      if(len>=2000){
	ERS_LOG("Filter string is too long (>2000 characters) ignoring");
	return;
      }
      m_configRules->push_back(monsvc::ConfigurationRule::from(buff));
    }catch(boost::property_tree::ptree_bad_path &ex){
      ERS_LOG("Warning got "<<ex.what()<<" while parsing OHRule");
    }catch(ers::Issue &ex){
      ERS_LOG("Warning caught issue "<<ex.what());
    }
  }
  auto ISRule=args.get_child_optional("Parameters.ISPublishingParameters");  
  if(ISRule){
    try{
      unsigned int interval=args.get("Parameters.ISPublishingParameters.PublishInterval",60);
      std::string serverName=std::string(args.get_child("Parameters.ISPublishingParameters.ISServer").data());
      char buff[2000];
      int len=snprintf(buff,2000,"%s:%s/%s=>is:(%d,%s)",
		       filtName.c_str(),
		       incFilter.c_str(),
		       excFilter.c_str(),
		       interval,
		       serverName.c_str()
		       );
      if(len>=2000){
	ERS_LOG("Filter string is too long (>2000 characters) ignoring");
	return;
      }
      m_configRules->push_back(monsvc::ConfigurationRule::from(buff));
    }catch(boost::property_tree::ptree_bad_path &ex){
      ERS_LOG("Warning unkown config type! discarding!");
    }catch(ers::Issue &ex){
      ERS_LOG("Warning caught issue "<<ex.what());
    }
  }
}

bool HLTMP::MonSvcInfoService::beginEvent(const boost::property_tree::ptree &){

  return false;
}

bool HLTMP::MonSvcInfoService::endEvent(const boost::property_tree::ptree &){
  auto itend=m_objMap->end();
  for(auto it=m_objMap->begin();it!=itend;++it){
    auto holder=it->second;
    auto obj=holder->cont;
    auto isObj=holder->isInfo;
    //ERS_LOG("Publishing object "<<obj->getObjName());
    monsvc::ptr<ISInfoDynAny>::scoped_lock lock(holder->monsvc_ptr);
    if(holder->published){
      findDiff(holder->lastCopy,obj);
    }
    // copy INT values
    //ERS_LOG("Copying int values "<<obj->getObjName());
    for(size_t t=0;t<holder->intFieldMap.size();t++){

      // ERS_LOG("Copying int values at field pos="<<t<<" isPos="<<holder->intFieldMap[t]
      // 	      <<" name="<<obj->getFieldNames(GenericHLTContainer::INT)[t]<<" ISname="<<isObj->getAttributeDescription(holder->intFieldMap[t]).name());
      switch(isObj->getAttributeType(holder->intFieldMap[t])){
      case ISType::Boolean:
	isObj->getAttributeValue<bool>((holder->intFieldMap[t]))=obj->getIntField(t);
	break;
      case ISType::S8:
	isObj->getAttributeValue<int8_t>((holder->intFieldMap[t]))=obj->getIntField(t);
	break;
      case ISType::U8:
	isObj->getAttributeValue<uint8_t>((holder->intFieldMap[t]))=obj->getIntField(t);
	break;
      case ISType::S16:
	isObj->getAttributeValue<int16_t>((holder->intFieldMap[t]))=obj->getIntField(t);
	break;
      case ISType::U16:
	isObj->getAttributeValue<uint16_t>((holder->intFieldMap[t]))=obj->getIntField(t);
	break;
      case ISType::S32:
	isObj->getAttributeValue<int32_t>((holder->intFieldMap[t]))=obj->getIntField(t);
	break;
      case ISType::U32:
	isObj->getAttributeValue<uint32_t>((holder->intFieldMap[t]))=obj->getIntField(t);
	break;
      case ISType::S64:
	isObj->getAttributeValue<int64_t>((holder->intFieldMap[t]))=obj->getIntField(t);
	break;
      case ISType::U64:
	isObj->getAttributeValue<uint64_t>((holder->intFieldMap[t]))=obj->getIntField(t);
	break;
      default:
	break;
      }
    }
    //    ERS_LOG("Copying float values "<<obj->getObjName());
    //copy FP/DP values
    for(size_t t=0;t<holder->floatFieldMap.size();t++){
      switch(isObj->getAttributeType(holder->floatFieldMap[t])){
      case ISType::Float:
	isObj->getAttributeValue<float>((holder->floatFieldMap[t]))=obj->getFloatField(t);
	break;
      case ISType::Double:
	isObj->getAttributeValue<double>((holder->floatFieldMap[t]))=obj->getFloatField(t);
	break;
      default:
	break;
      }
    }
    // copy INT vectors
    for(size_t t=0;t<holder->intVecFieldMap.size();t++){
      switch(isObj->getAttributeType(holder->intVecFieldMap[t])){
      case ISType::Boolean:
	HLTMP::setVector(isObj->getAttributeValue<std::vector<bool> >((holder->intVecFieldMap[t])),obj->getIntVecField(t));
	break;
      case ISType::S8:
	HLTMP::setVector(isObj->getAttributeValue<std::vector<char> >((holder->intVecFieldMap[t])),obj->getIntVecField(t));
	break;
      case ISType::U8:
	HLTMP::setVector(isObj->getAttributeValue<std::vector<unsigned char> >((holder->intVecFieldMap[t])),obj->getIntVecField(t));
	break;
      case ISType::S16:
	HLTMP::setVector(isObj->getAttributeValue<std::vector<short> >((holder->intVecFieldMap[t])),obj->getIntVecField(t));
	break;
      case ISType::U16:
	HLTMP::setVector(isObj->getAttributeValue<std::vector<unsigned short> >((holder->intVecFieldMap[t])),obj->getIntVecField(t));
	break;
      case ISType::S32:
	HLTMP::setVector(isObj->getAttributeValue<std::vector<int> >((holder->intVecFieldMap[t])),obj->getIntVecField(t));
	break;
      case ISType::U32:
	HLTMP::setVector(isObj->getAttributeValue<std::vector<unsigned int> >((holder->intVecFieldMap[t])),obj->getIntVecField(t));
	break;
      case ISType::S64:
	HLTMP::setVector(isObj->getAttributeValue<std::vector<int64_t> >((holder->intVecFieldMap[t])),obj->getIntVecField(t));
	break;
      case ISType::U64:
	HLTMP::setVector(isObj->getAttributeValue<std::vector<uint64_t> >((holder->intVecFieldMap[t])),obj->getIntVecField(t));
	break;
      default:
	break;
      }
    }
    //copy FP/DP vectors
    for(size_t t=0;t<holder->floatVecFieldMap.size();t++){
      switch(isObj->getAttributeType(holder->floatVecFieldMap[t])){
      case ISType::Float:
	HLTMP::setVector(isObj->getAttributeValue<std::vector<float> >(holder->floatVecFieldMap[t]),obj->getFloatVecField(t));
	break;
      case ISType::Double:
	HLTMP::setVector(isObj->getAttributeValue<std::vector<double> >(holder->floatVecFieldMap[t]),obj->getFloatVecField(t));
	break;
      default:
	break;
      }
    }
    *(holder->lastCopy)=*obj;//copy last instance
  }
  return true;
}

std::vector<ContPtr> HLTMP::MonSvcInfoService::queryISRegistry(const std::string& /*regex*/){
  return std::vector<ContPtr>();
}

bool HLTMP::MonSvcInfoService::releaseObject(const std::string fullName){
  auto it=m_objMap->find(fullName);
  if(it==m_objMap->end())return false;
  m_objMap->erase(it);
  return true;
}

bool HLTMP::MonSvcInfoService::registerObject(const std::string publishPath,
					      ContPtr obj){
  std::string fullName;
  if(publishPath.back()=='/'){
    fullName=publishPath+obj->getObjName();
  }else{
    fullName=publishPath+obj->getObjName();
  }
  if(m_objMap->find(fullName)!=m_objMap->end()){
    return false;
  }
  ERS_LOG(" SAMI SAMI Calling register object with publishPath "<<publishPath<<" objName "<<obj->getObjName());
  ISInfoDynAny *isObj=0;
  try{
    isObj=new ISInfoDynAny(*m_part,obj->getTypeName());
  }catch(daq::is::DocumentNotFound &ex){
    //should this fallback to GenericHLTContainer type?
    std::cerr<<HLTMP::getTimeTag()<<"Incorrect type "<<obj->getTypeName();
    throw std::runtime_error(std::string("Incorrect type ")+obj->getTypeName());
  }
  std::shared_ptr<InfoHolder> holder=0;
  if(isObj){
    holder=std::make_shared<InfoHolder>();
    //map int fields
    auto names=obj->getFieldNames(hltinterface::GenericHLTContainer::INT);
    holder->intFieldMap.resize(names.size());
    for(size_t k=0;k<names.size();k++){
      bool found=false;
      std::string n=names.at(k);
      for(size_t t=0;t<isObj->getAttributesNumber();t++){
	try{
	  auto attrib =isObj->getAttributeDescription(t);
	  if(attrib.isArray()){
	    throw std::runtime_error(std::string("Field named ")+n+std::string(" is declared as array in ISType ")+obj->getTypeName()+" but declared as non-array in container object");
	  }
	  if(n==attrib.name()){
	    found=true;
	    holder->intFieldMap.at(k)=t;
	  }
	}catch(daq::is::AttributeNotFound &ex){
	  std::cerr<<HLTMP::getTimeTag()<<"IncorrectFieldName "<<obj->getTypeName();	  
	}
      }
      if(!found){
	throw std::runtime_error(std::string("Field named ")+n+std::string(" does not exist in ISType ")+obj->getTypeName());
      }
    }
    // Map float fields;
    names=obj->getFieldNames(hltinterface::GenericHLTContainer::FLOAT);
    holder->floatFieldMap.resize(names.size());
    for(size_t k=0;k<names.size();k++){
      bool found=false;
      std::string n=names.at(k);
      for(size_t t=0;t<isObj->getAttributesNumber();t++){
	try{
	  auto attrib =isObj->getAttributeDescription(t);
	  if(attrib.isArray()){
	    throw std::runtime_error(std::string("Field named ")+n+std::string(" is declared as array in ISType ")+obj->getTypeName()+" but declared as non-array in container object");
	  }
	  if(n==attrib.name()){
	    found=true;
	    holder->floatFieldMap.at(k)=t;
	  }
	}catch(daq::is::AttributeNotFound &ex){
	  std::cerr<<HLTMP::getTimeTag()<<"IncorrectFieldName "<<obj->getTypeName();	  
	}
      }
      if(!found){
	throw std::runtime_error(std::string("Field named ")+n+std::string(" does not exist in ISType ")+obj->getTypeName());
      }
    }
    //map int vectors
    names=obj->getFieldNames(hltinterface::GenericHLTContainer::INTVEC);
    holder->intVecFieldMap.resize(names.size());
    for(size_t k=0;k<names.size();k++){
      bool found=false;
      std::string n=names.at(k);
      for(size_t t=0;t<isObj->getAttributesNumber();t++){
	try{
	  auto attrib =isObj->getAttributeDescription(t);
	  if(!attrib.isArray()){
	    throw std::runtime_error(std::string("Field named ")+n+std::string(" is declared as non-array in ISType ")+obj->getTypeName()+" but declared as array in container object");
	  }
	  if(n==attrib.name()){
	    found=true;
	    holder->intVecFieldMap.at(k)=t;
	  }
	}catch(daq::is::AttributeNotFound &ex){
	  std::cerr<<HLTMP::getTimeTag()<<"IncorrectFieldName "<<obj->getTypeName();	  
	}
      }
      if(!found){
	throw std::runtime_error(std::string("Field named ")+n+std::string(" does not exist in ISType ")+obj->getTypeName());
      }
    }
    //map Float vectors
    names=obj->getFieldNames(hltinterface::GenericHLTContainer::FLOATVEC);
    holder->floatVecFieldMap.resize(names.size());
    for(size_t k=0;k<names.size();k++){
      bool found=false;
      std::string n=names.at(k);
      for(size_t t=0;t<isObj->getAttributesNumber();t++){
	try{
	  auto attrib =isObj->getAttributeDescription(t);
	  if(!attrib.isArray()){
	    throw std::runtime_error(std::string("Field named ")+n+std::string(" is declared as non-array in ISType ")+obj->getTypeName()+" but declared as array in container object");
	  }
	  if(n==attrib.name()){
	    found=true;
	    holder->floatVecFieldMap.at(k)=t;
	  }
	}catch(daq::is::AttributeNotFound &ex){
	  std::cerr<<HLTMP::getTimeTag()<<"IncorrectFieldName "<<obj->getTypeName();	  
	}
      }
      if(!found){
	throw std::runtime_error(std::string("Field named ")+n+std::string(" does not exist in ISType ")+obj->getTypeName());
      }
    }
  }
  if(holder){
    holder->cont=obj;
    holder->isInfo=isObj;
    holder->published=false;
    holder->lastCopy=m_factory->cloneObject(obj);
    m_objMap->insert(std::make_pair(fullName,holder));
    holder->monsvc_ptr=monsvc::MonitoringService::instance().register_object(fullName,isObj,&MonSvcInfoService::monsvcCallback);
    if(obj->getPolicy()==hltinterface::GenericHLTContainer::PerLB){
      m_lbList->push_back(holder);
    }else if(obj->getPolicy()==hltinterface::GenericHLTContainer::PerPublication){
      m_perPublishList->push_back(holder);
    }
    return true;
  }else{
    throw std::runtime_error(std::string("Something went wrong! objName=")+obj->getObjName()+" Typename"+obj->getTypeName());
    return false;
  }
  return false;
}

void HLTMP::MonSvcInfoService::monsvcCallback(const std::string& name,ISInfoDynAny*){
  auto it=m_objMap->find(name);
  if(it!=m_objMap->end()){
    it->second->published=true;
  }
}

void HLTMP::MonSvcInfoService::findDiff(ContPtr last,ContPtr update){
  auto prop=update->getFieldProperties(hltinterface::GenericHLTContainer::INTVEC);
  for(size_t t=0;t<prop.size();t++){
    auto p=prop.at(t);
    auto uVec=update->getIntVecField(t);
    auto lVec=last->getIntVecField(t);
    if(p==hltinterface::GenericHLTContainer::ACCUMULATOR){
      if(lVec.size()<=uVec.size()){
	uVec.erase(uVec.begin(),uVec.begin()+lVec.size());
      }
    }
  }

  prop=update->getFieldProperties(hltinterface::GenericHLTContainer::FLOATVEC);
  for(size_t t=0;t<prop.size();t++){
    auto p=prop.at(t);
    auto uVec=update->getFloatVecField(t);
    auto lVec=last->getFloatVecField(t);
    if(p==hltinterface::GenericHLTContainer::ACCUMULATOR){
      if(lVec.size()<=uVec.size()){
	uVec.erase(uVec.begin(),uVec.begin()+lVec.size());
      }
    }
  }
}

void HLTMP::MonSvcInfoService::setModification(bool b){
  p_setModification(b);
}

bool HLTMP::MonSvcInfoService::pullInfo(const std::string &publishPath,const std::string &objectName){
  return false;
}
