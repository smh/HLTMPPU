#include "HLTMPPU/DFFileSession.h"
#include "HLTMPPU/DFFileEvent.h"
#include "HLTMPPU/eformat_utils.h"
#include "ers/ers.h"
#include "eformat/eformat.h"
#include "eformat/index.h"
#include "eformat/write/eformat.h"
#include "eformat/SourceIdentifier.h"
#include "eformat/FullEventFragmentNoTemplates.h"
#include "EventStorage/DataReader.h"
#include "EventStorage/pickDataReader.h"
#include "EventStorage/DataWriter.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>

extern "C" std::unique_ptr<daq::dfinterface::Session> createSession(const boost::property_tree::ptree &conf){
  std::unique_ptr<daq::dfinterface::Session> s(new HLTMP::DFFileSession(conf));
  return s;
}

void HLTMP::DFFileSession::open(const std::string& /*clientName*/){
  ERS_DEBUG(1,"Opening File");
  nextFile();

  // Skipping requested number of events from the first file
  uint eventsInFile=m_currReader->eventsInFile();
  if (m_nSkip >= eventsInFile ) {
      std::string msg = "Events to skip: " + std::to_string(m_nSkip) +  ", total events in file: " + std::to_string(eventsInFile);
      throw daq::dfinterface::BadConfiguration(ERS_HERE,msg);
  }
  if (m_nSkip) {
    ERS_LOG("Skipping " << m_nSkip << " events");
  }
  for(uint i=0; i<m_nSkip ; i++){
    auto blob=getNextEvent();
    m_currEventInFile++;
  }
}

bool HLTMP::DFFileSession::isOpen() const{
  return static_cast<bool>(m_currReader);
}

void HLTMP::DFFileSession::close(){

}

std::unique_ptr<daq::dfinterface::Event> HLTMP::DFFileSession::getNext(){
  ERS_DEBUG(3,"Enter, event: " << m_nEvents);
  if((m_nMaxEvents>0) && (m_nEvents>=m_nMaxEvents)){
    std::string message = "Event count reached, max events to read: " + std::to_string(m_nMaxEvents);
    ERS_LOG(message);
    throw daq::dfinterface::NoMoreEvents(ERS_HERE, message);
  }

  uint eventsInFile=m_currReader->eventsInFile();
  std::unique_ptr<uint32_t[]> blob;
  if((uint)(m_currEventInFile+m_stride)<eventsInFile){
    unsigned int target=m_currEventInFile+m_stride-1;
    while((uint)(m_currEventInFile)<target){//skip events      
      auto evt = getNextEvent();
      m_currEventInFile++;
    }
    blob=getNextEvent();
    m_currEventInFile++;
  }else{
    int newoffset=m_currEventInFile+m_stride-eventsInFile;
    if(nextFile()){
      eventsInFile=m_currReader->eventsInFile();
      if(eventsInFile>(uint)newoffset){
	if(newoffset>0){
	  skipEvents(newoffset-1);
	}
	blob=getNextEvent();
	m_currEventInFile=newoffset;
      }else{
	throw daq::dfinterface::NoMoreEvents(ERS_HERE,"No more events");
      }
    }else{
	throw daq::dfinterface::NoMoreEvents(ERS_HERE,"No more events");
    }
  }
  m_nEvents++;
  auto dffileevent = std::make_unique<HLTMP::DFFileEvent>(std::move(blob));
  // Add the extra L1 ROBs requested to be read as part of L1 Results
  dffileevent->setExtraL1Robs(m_extraL1Robs);
  ERS_DEBUG(3,"Exit");
  return dffileevent;
}

std::unique_ptr<daq::dfinterface::Event> HLTMP::DFFileSession::tryGetNextUntil(
									       const std::chrono::steady_clock::time_point& /*absTime*/){
  return std::unique_ptr<daq::dfinterface::Event>();
}

std::unique_ptr<daq::dfinterface::Event> HLTMP::DFFileSession::tryGetNextFor(
									     const std::chrono::steady_clock::duration& /*relTime*/){
  return std::unique_ptr<daq::dfinterface::Event>();
}

void HLTMP::DFFileSession::accept(std::unique_ptr<daq::dfinterface::Event> event /*event*/,
                                  std::unique_ptr<uint32_t[]> hltr) {
  if (!m_outFileName.empty()) {
    DFFileEvent* df_file_event = dynamic_cast<DFFileEvent*>(event.get());

    std::vector<uint32_t> finalEvent;
    try {
      finalEvent = merge_hltresult_with_input_event(hltr.get(), df_file_event->getCurrentEvent()->start(), m_comp, m_compLevel);
    } catch (std::exception & ex) {
      ERS_LOG("Problem creating final Event for gid " << df_file_event->gid() << " to be written to disk: " << ex.what());
      return;
    }
    auto finalSize = finalEvent.size();
    auto sizeInBytes = finalSize*4;  // finalSize is in words(4 byte chunks), putData argument is bytes
    ERS_DEBUG(2, "Writing " << sizeInBytes << " bytes ");
    auto wres=m_writer->putData(sizeInBytes,finalEvent.data());
    if(wres){
      ERS_LOG("Writing event failed");
    }
  }
}

void HLTMP::DFFileSession::reject(std::unique_ptr<daq::dfinterface::Event> /*event*/){

}


bool HLTMP::DFFileSession::nextFile(){
  if(m_fileNames.size()==0){
    throw daq::dfinterface::BadConfiguration(ERS_HERE,"No input files defined in configuration");
    return false;
  }
  ERS_DEBUG(1, "m_loopFiles: " << m_loopFiles);
  if(m_currFile>=(int)m_fileNames.size()){
    if(m_loopFiles){
      m_currFile=-1;
    }else{
      throw daq::dfinterface::NoMoreEvents(ERS_HERE,"No more Files to read from");
      return false;
    }
  }
  if(m_currFile<(int)m_fileNames.size()){
    ERS_LOG("opening new file");
    if(m_currReader){
      m_currReader.reset();
    }
    m_currFile++;
    if(m_currFile==(int)m_fileNames.size()){
      if(!m_loopFiles){
	throw daq::dfinterface::NoMoreEvents(ERS_HERE,"No more Files to read from");
	return false;
      }else{
	m_currFile=0;
      }
    }
    try{
      std::unique_ptr<EventStorage::DataReader> temp (pickDataReader(m_fileNames.at(m_currFile)));
      m_currReader = std::move(temp);
    }catch(ers::Issue &e){
      ERS_LOG("Caught issue "<<e.what());
    }
    if (!m_currReader) {
      ERS_LOG("Failed to open file \""<<m_fileNames.at(m_currFile)<<"\" good() call= "<<m_currReader->good());
      throw daq::dfinterface::BadConfiguration(ERS_HERE,std::string("Failed to open file ")+m_fileNames.at(m_currFile));
      return false;
    }
    ERS_DEBUG(1,"Opened file \""<<m_fileNames.at(m_currFile)<<"\" good() call= "<<m_currReader->good()); 
    return true;
  }
  ERS_DEBUG(1,"m_currFile="<<m_currFile<<", m_fileNames.size()="<<m_fileNames.size());
  throw daq::dfinterface::NoMoreEvents(ERS_HERE,"No more Files to read from");
  return false;
}


std::unique_ptr<uint32_t[]> HLTMP::DFFileSession::getNextEvent(){
  char *buff;
  unsigned int size=0;
  try{
    int error_code=m_currReader->getData(size,&buff);
    while (error_code == DRWAIT) {
      usleep(500000);
      ERS_INFO(" Waiting for more data.");
      error_code = m_currReader->getData(size, &buff);
    }
    if (error_code == DRNOOK) {
      ERS_INFO(" Reading of data NOT OK!");
      delete buff;
      throw daq::dfinterface::OperationTimedOut(ERS_HERE,"Can't read event from the file");
      return 0;
    }
    if (error_code == DROK) {
      ERS_DEBUG(1, " Event OK");
    }
  } catch(std::exception &ex){
    ERS_LOG("Failed reading event. Caught exception \""<<ex.what()<<"\"");
    std::cerr<<"Reading event failed. Caught exception \""<<ex.what()<<"\""<<std::endl;
  }
  uint32_t *blob=reinterpret_cast<uint32_t*>(buff);
  return std::unique_ptr<uint32_t[]>(blob);
}

bool HLTMP::DFFileSession::skipEvents(uint num){
  for(uint i=0;i<num;i++){
    auto evt=getNextEvent();
  }
  return true;
}

HLTMP::DFFileSession::~DFFileSession(){ }

HLTMP::DFFileSession::DFFileSession(const boost::property_tree::ptree &cargs){
  boost::property_tree::ptree args=cargs.get_child("Configuration.HLTMPPUApplication.DataSource.HLTDFFileBackend");
  m_start=args.get("start_id",1);
  m_stride=args.get("stride",1);
  m_currFile=args.get("fileOffset",-1);
  m_loopFiles=(args.get("loopOverFiles","false")!="False");
  m_nMaxEvents=args.get("numEvents",-1);
  m_currEventInFile=0;
  m_nEvents=0;
  m_nSkip=args.get("skipEvents",0);
  m_comp=eformat::UNCOMPRESSED;
  m_compLevel = 0;
  ERS_LOG("Configuring DFFileSession, numEvents="<< m_nMaxEvents);
  try{
    auto of=args.get_child("outputFileName");
    m_outFileName=std::string(of.data());
    ERS_LOG("Output file name "<<m_outFileName);
  }catch(boost::property_tree::ptree_bad_path &ex){
    ERS_DEBUG(1,"Output file name is not specified");
    ERS_LOG("Failed to get outputFileName");
  }
  try{
    auto of=args.get_child("compressionFormat");
    std::string ctype(of.data());
    if(ctype=="ZLIB") {
      m_comp=eformat::ZLIB;
      m_compLevel=args.get("compressionLevel",2);
    }
  }catch(boost::property_tree::ptree_bad_path &ex){
    ERS_DEBUG(1,"Compression is not specified");
    ERS_LOG("Failed to get Compression information");
  }
  auto r = args.get_child("fileList").equal_range("file");
  BOOST_FOREACH(const boost::property_tree::ptree::value_type &v, r) {
    m_fileNames.push_back(v.second.get_value<std::string>());
    ERS_LOG("Adding file "+v.second.get_value<std::string>());
  }
  if(m_fileNames.size()==0){
    ERS_LOG("No input files specified");
  }

  // Add each ROBID in extraL1Robs
  auto robit = args.get_child("extraL1Robs").equal_range("ROBID");
  BOOST_FOREACH(const boost::property_tree::ptree::value_type &v, robit) {
    m_extraL1Robs.push_back(v.second.get_value<uint32_t>());
    ERS_DEBUG(1, " Adding Extra L1 ROB " << v.second.get_value<uint32_t>());
  }

  ERS_LOG("Initializing DataWriter");
    std::unique_ptr<EventStorage::run_parameters_record> m_runParams;
  m_runParams=std::make_unique<EventStorage::run_parameters_record>();
  m_runParams->run_number = 1234123412;
  m_runParams->max_events = 0;
  m_runParams->rec_enable = 0;
  m_runParams->trigger_type = 0;
  m_runParams->detector_mask_LS = 0;
  m_runParams->detector_mask_MS = 0;
  m_runParams->beam_type = 0;
  m_runParams->beam_energy = 0;
  if(!m_outFileName.empty()){
    char buff[2000];
    m_start = args.get("start_id",1);
    snprintf(buff,2000,"%s_Child-%03d",m_outFileName.c_str(),m_start);
    ERS_LOG("Opening outputfile "<<buff);
    m_writer=std::make_unique<EventStorage::DataWriter>(std::string("."),//use work Directory right now
							std::string(buff),
							*m_runParams,
							std::vector<std::string>(),
							1);
    m_writer->setMaxFileMB(2000);
  }
}

