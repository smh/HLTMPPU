
#include "HLTMPPU/HLTMPPU.h"
// needed for system calls
#include <errno.h>
#include <sys/prctl.h>
#include <signal.h>
#include <sys/wait.h>
#include <cstdio>
#include <cstdlib>
#include <dlfcn.h>
#include <unistd.h>

// needed for io, threads etc
#include <string>
#include <cstring>
#include <iostream>
#include <functional>
#include <boost/dll.hpp>
#include <boost/thread.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>
#include <ers/ers.h>
#include "HLTMPPU/Issues.h"
//needed for IPC initialization
#include "ipc/core.h"
//InformationService
//#include "HLTMPPU/InfoService.h"
#include "hltinterface/IInfoRegister.h"
// Data Source
#include "HLTMPPU/DataSource.h"
//ISObjects
#include "HLTMPPU/HLTMPPUInfo.h"
#include "HLTMPPU/HLTMPPUMotherInfo.h"
//needed temporarily for IS publication.
#include "is/infoT.h"
#include "is/infodictionary.h"
#include "oh/OHRootProvider.h"
#include "TH1F.h"
#include "dirent.h"
//Random number generator
#include <random>
#include "HLTMPPU/HLTMPPU_utils.h"
extern char **environ;

namespace HLTPU {
  //fix this!
  class ScopedISHelper{
  public:
    ScopedISHelper(long LB,long ev){
      m_tree.put("eventNumber",ev);
      m_tree.put("LBNumber",LB);
      try{
	hltinterface::IInfoRegister::instance()->beginEvent(m_tree);
      }catch(const std::exception &ex){
	ERS_LOG("Caught exception while calling beginEvent for event "
		<<ev<<" at LB "<<LB
		<<". Exception was "<<ex.what());
      }
    }
    ~ScopedISHelper(){
      try{
	hltinterface::IInfoRegister::instance()->endEvent(m_tree);
      }catch(const std::exception &ex){
	ERS_LOG("Caught exception while calling beginEvent for event "<<m_tree.get<long>("eventNumber")
		<<" at LB "<<m_tree.get<long>("LBNumber")
		<<". Exception was "<<ex.what());
      }
    }
  private:
    boost::property_tree::ptree m_tree;
  };
}

HLTMPPU::HLTMPPU(){
  m_myPid=getpid();
  m_myPgid=getpgid(0);
  ERS_LOG("Constructing HLTMPPU Interface with pid "<<m_myPid<<" and pgid "<<m_myPgid);
  ERS_LOG("Library built on "<<__DATE__<<" "<<__TIME__<<std::endl);
  m_nannyWork=false;
  m_numChildren=1;
  m_HLTSteering=0;
  m_dataSource=0;
  m_myName=getenv("TDAQ_APPLICATION_NAME");
  m_processEvents=true;
  m_childLogPath="/tmp/";
  // to be removed
  char * tmp = getenv("TDAQ_IS_SERVER");
  if(tmp){
    m_ISSName=std::string(tmp);
  }else{
    m_ISSName="DF";
  }
  m_publishInterval=30;
  m_motherInfo=std::make_shared<HLTMP::HLTMPPUMotherInfo>();
  m_forkDelay=0;
  m_preforkSleep=0;
  m_terminationStarted=false;
  m_dumpFD=false;
  m_dumpThreads=false;
  m_saveConfigOpts=false;
  m_FinalizeTimeout=120;
  m_infoService=0;
  m_keepNumForks=false;
  m_termStagger=0;
  m_skipFinalizeWorker=true;
  m_exitImmediately=false;
  m_dontPublishIS=false;
  m_allowedExtraThreadsAtFork = 0;
  m_threadWaitTimeout = 10;
}

HLTMPPU::~HLTMPPU(){
  ERS_DEBUG(1, "pid=" << m_myPid << ", Destructing HLTMPPU Interface");
  stopNanny();
  stopMotherPublisher();

}

bool HLTMPPU::configure(const boost::property_tree::ptree& args ) {
  ERS_LOG("pid=" << m_myPid << ", using the following configuration tree: ");
  printPtree(args,"");
  std::cout.flush();
  m_configTree=args;
  m_numChildren=args.get("Configuration.HLTMPPUApplication.numForks",4);
  m_FinalizeTimeout=args.get("Configuration.HLTMPPUApplication.finalizeTimeout",120);
  m_childLogPath=std::string(args.get_child("Configuration.Partition.LogRoot").data());
  m_childLogName=args.get("Configuration.HLTMPPUApplication.childLogName","");
  m_dontPublishIS=args.get("Configuration.HLTMPPUApplication.dontPublishIS",0);


  try{
    auto extras=args.get_child("Configuration.HLTMPPUApplication.extraParams").equal_range("parameter");

    /// Lambda function to add extra parameters.
    /// \param variable Reference to member variable
    /// \param expected_name Name in ptree that refers to that variable. If this name doesn't
    ///        match the name in ptree, nothing will be done
    /// \param ptree_data ptree element that should be formed as "expected_name=value"
    /// \param minval minimum allowed value for variable. Set to 0 for unsigned variables
    auto tryAddExtraParam = [&](auto & variable, std::string expected_name, std::string ptree_data, int minval=0) {
      std::vector<std::string> tokens;
      boost::algorithm::split(tokens, ptree_data, boost::is_any_of("="));  // Split text into 2 parts with = in between
      if (tokens.size()>1) {
        if (tokens.at(0) == expected_name) {  // tokens.at(0) is the variable name
          try {
            int value = std::max(std::stoi(tokens.at(1)), minval);  // tokens.at(1) is the value
            ERS_LOG("Setting extraParam " << expected_name << " = " << value);
            variable = value;
          } catch (std::exception & ex) {
            ERS_LOG(expected_name << " is set incorrectly! offending line: \"" << ptree_data << "\", exception : " << ex.what());
          }
        }
      }
    };
    for(auto it=extras.first;it!=extras.second;++it) {
      std::string data=std::string(it->second.data());

      tryAddExtraParam(m_forkDelay, "forkDelay", data);
      tryAddExtraParam(m_preforkSleep, "preForkSleep", data);
      tryAddExtraParam(m_publishInterval, "publishInterval", data);
      tryAddExtraParam(m_keepNumForks, "keepForks", data);
      tryAddExtraParam(m_dumpFD, "dumpFDs", data);
      tryAddExtraParam(m_dumpThreads, "dumpThreads", data);
      tryAddExtraParam(m_saveConfigOpts, "SaveInputParams", data);
      tryAddExtraParam(m_skipFinalizeWorker, "SkipFinalizeWorker", data);
      tryAddExtraParam(m_exitImmediately, "ExitWithoutCleanup", data);
      tryAddExtraParam(m_termStagger, "MaxTermStagger_s", data);
      tryAddExtraParam(m_allowedExtraThreadsAtFork, "allowedThreadsAtFork", data);
      tryAddExtraParam(m_threadWaitTimeout, "threadWaitTimeout", data);
    }
  }catch(boost::property_tree::ptree_bad_path &ex){
    ERS_DEBUG(1,"there is no extraParams, skipping");
  }catch (...){
    std::cerr<<HLTMP::getTimeTag()<<"I caught something that I don't know (i.e. catch(...) caught the exception) "<<std::endl;
  }
  ERS_DEBUG(1,"Configuring. my pid is "<<m_myPid);

  ERS_LOG("Loading DataSource library");
  // If DataSourceLibrary does not contain symbol create_hltmp_datasource, it means it contains
  // dfinterface implementation instead. In this case DFDataSource is used
  std::string dslibrary = args.get_child("Configuration.HLTMPPUApplication.DataSourceLibrary").data();
  {
    boost::dll::shared_library temp;
    try {
      temp = boost::dll::shared_library("lib" + dslibrary + ".so",
                                        boost::dll::load_mode::type::search_system_folders |
                                        boost::dll::load_mode::type::rtld_local);
    } catch (boost::system::system_error & ex) {
      std::string errMsg=std::string("DataSource library load failed with \"")+ex.what()+"\";";
      ers::fatal(HLTMPPUIssue::DLLIssue(ERS_HERE,errMsg.c_str()));
      throw HLTMPPUIssue::DLLIssue(ERS_HERE,errMsg.c_str());
      return false;
    }

    if (!temp.has("create_hltmp_datasource")) {
      ERS_LOG("Can't find symbol create_hltmp_datasource in '"<< dslibrary <<"', selecting DFDataSource ");
      dslibrary = "DFDataSource";
    }
  }

  try {
    m_datasource_library = boost::dll::shared_library("lib" + dslibrary + ".so",
                                                      boost::dll::load_mode::type::search_system_folders |
                                                      boost::dll::load_mode::type::rtld_global);
  } catch (boost::system::system_error & ex) {
    std::string errMsg=std::string("DataSource library load failed with \"")+ex.what()+"\";";
    ers::fatal(HLTMPPUIssue::DLLIssue(ERS_HERE,errMsg.c_str()));
    throw HLTMPPUIssue::DLLIssue(ERS_HERE,errMsg.c_str());
    return false;
  }

  ERS_LOG("Loading InformationService library");
  std::string islibrary = args.get_child("Configuration.HLTMPPUApplication.InfoServiceLibrary").data();
  try {
    m_infoservice_library = boost::dll::shared_library("lib" + islibrary + ".so",
                                                       boost::dll::load_mode::type::search_system_folders |
                                                       boost::dll::load_mode::type::rtld_global);
  } catch (boost::system::system_error & ex) {
    std::string errMsg=std::string("InfoService library load failed with \"")+ex.what()+"\";";
    ers::fatal(HLTMPPUIssue::DLLIssue(ERS_HERE,errMsg.c_str()));
    throw HLTMPPUIssue::DLLIssue(ERS_HERE,errMsg.c_str());
    return false;
  }

  //load information service
  //load HLTImplementation libraries
  std::vector<std::string> hlt_library_names;
  ERS_LOG("Loading HLT libraries:");
  BOOST_FOREACH(const boost::property_tree::ptree::value_type &v,
		args.get_child("Configuration.HLTMPPUApplication.HLTImplementationLibraries")){
    ERS_LOG("... " << v.second.data());
    hlt_library_names.push_back(v.second.data());
    try {
      m_hlt_libraries.emplace_back("lib" + hlt_library_names.back() + ".so",
                                   boost::dll::load_mode::type::search_system_folders |
                                   boost::dll::load_mode::type::rtld_global);
    } catch (boost::system::system_error & ex) {
      std::string errMsg=std::string("HLT library load failed for " + hlt_library_names.back() + " with \"")+ex.what()+"\";";
      ers::fatal(HLTMPPUIssue::DLLIssue(ERS_HERE,errMsg.c_str()));
      throw HLTMPPUIssue::DLLIssue(ERS_HERE,errMsg.c_str());
      return false;
    }
  }

  //construct the objects
  using hltcreator = hltinterface::HLTInterface*();
  using dscreator = HLTMP::DataSource* ();
  using isvccreator = hltinterface::IInfoRegister* ();
  ERS_DEBUG(1,"Instantiating DataSource implementation");
  std::function<dscreator> dsc;
  try {
    dsc = m_datasource_library.get<dscreator>("create_hltmp_datasource");
  } catch (std::exception & ex) {
    std::cerr<<HLTMP::getTimeTag()<<"Can't get DataSource factory function. Check configuration! Can't continue. exiting"<<std::endl;
    ers::fatal(HLTMPPUIssue::ConfigurationIssue(ERS_HERE,"Can't get DataSource factory function. Check configuration! Can't continue. exiting"));
    throw HLTMPPUIssue::ConfigurationIssue(ERS_HERE,"Can't get DataSource factory function. Check configuration! Can't continue. exiting");
    return false;
  }
  m_dataSource=dsc();

  ERS_DEBUG(1,"Instantiating Info Service implementation");
  std::function<isvccreator> isc;
  try {
    isc = m_infoservice_library.get<isvccreator>("create_hltmp_infoservice");
  } catch (std::exception & ex) {
    std::cerr<<HLTMP::getTimeTag()<<"Can't get InfoService factory function. Check configuration! Can't continue. exiting"<<std::endl;
    ers::fatal(HLTMPPUIssue::ConfigurationIssue(ERS_HERE,"Can't get InfoService factory function. Check configuration! Can't continue. exiting"));
    throw HLTMPPUIssue::ConfigurationIssue(ERS_HERE,"Can't get InfoService factory function. Check configuration! Can't continue. exiting");
    return false;
  }

  m_infoService=isc();
  ERS_DEBUG(1,"Instantiating HLTSteering implementation");
  std::function<hltcreator> hltc;
  for (auto & lib : m_hlt_libraries) {
    if (lib.has("hlt_factory")) {
      hltc = lib.get<hltcreator>("hlt_factory");
      ERS_LOG("Found hlt_factory in " << lib.location());
    }
  }
  if(!hltc){
    std::cerr<<HLTMP::getTimeTag()<<"Can't get HLTSteering factory function. Check configuration! Can't continue. exiting"<<std::endl;
    ers::fatal(HLTMPPUIssue::ConfigurationIssue(ERS_HERE,"Can't get HLTSteering factory function. Check configuration! Can't continue. exiting"));
    throw HLTMPPUIssue::ConfigurationIssue(ERS_HERE,"Can't get HLTSteering factory function. Check configuration! Can't continue. exiting");
    return false;
  }

  m_HLTSteering=hltc();
  bool retVal=true;

  //configure infoservice
  {
    ERS_DEBUG(1,"Configuring Info Service implementation");
    boost::property_tree::ptree conf=args.get_child("Configuration.HLTMPPUApplication.InfoService");
    try{
      boost::optional<const boost::property_tree::ptree&> extraParms=args.get_child_optional("Configuration.HLTMPPUApplication.extraParams");
      if(extraParms){
	conf.add_child("extraParams",(*extraParms));
      }
      try{
	retVal=retVal&&m_infoService->configure(conf);
      }catch(ers::Issue &ex){
	ers::fatal(HLTMPPUIssue::ConfigurationIssue(ERS_HERE," InfoService configuration failed! Check configuration",ex));
	throw HLTMPPUIssue::ConfigurationIssue(ERS_HERE," InfoService configuration failed! Check configuration",ex);
	return false;
      }	
      if(!retVal){
	ers::fatal(HLTMPPUIssue::ConfigurationIssue(ERS_HERE," InfoService configuration failed! Check configuration"));
	throw HLTMPPUIssue::ConfigurationIssue(ERS_HERE," InfoService configuration failed! Check configuration");
      }
    }catch(std::exception &ex){
      std::cerr<<HLTMP::getTimeTag()<<"Caught exception \""<<ex.what()<<"\" during InfoService configuration"<<std::endl; 
      std::string errMsg=std::string("InfoService configuration failed with \"")+ex.what()+"\" Check configuration";
      ers::fatal(HLTMPPUIssue::ConfigurationIssue(ERS_HERE,errMsg.c_str()));
      throw HLTMPPUIssue::ConfigurationIssue(ERS_HERE,errMsg.c_str());
      return false;
    }
  }

  //configure data source
  {
    ERS_DEBUG(1,"Configuring DataSource implementation");    
    // const boost::property_tree::ptree& conf=args.get_child("Configuration.HLTMPPUApplication.DataSource");
    // boost::property_tree::ptree ptemp(conf);
    try{
      // DummyDataSource cannot do EB without ROB info ??
      bool r=m_dataSource->configure(m_configTree);
      retVal=retVal&&r;
      if(!r){
	ers::fatal(HLTMPPUIssue::ConfigurationIssue(ERS_HERE," DataSource configuration failed! Check configuration"));
	throw HLTMPPUIssue::ConfigurationIssue(ERS_HERE," DataSource configuration failed! Check configuration");
	return false;
      }
    }catch(std::exception &ex){
      std::cerr<<HLTMP::getTimeTag()<<"Caught exception \""<<ex.what()<<"\" during DataSource configuration"<<std::endl; 
      std::string errMsg=std::string("DataSource configuration failed with \"")+ex.what()+"\" Check configuration";
      ers::fatal(HLTMPPUIssue::ConfigurationIssue(ERS_HERE,errMsg.c_str()));
      throw HLTMPPUIssue::ConfigurationIssue(ERS_HERE,errMsg.c_str());
      return false;
    }
  }

  // configure HLT
  {
    ERS_DEBUG(1,"Configuring HLTSteering implementation");
    try{
      bool r=m_HLTSteering->configure(args);
      retVal=retVal&&r;
      if(!r){
	ers::fatal(HLTMPPUIssue::ConfigurationIssue(ERS_HERE,"HLT configuration failed! Check configuration"));
	throw HLTMPPUIssue::ConfigurationIssue(ERS_HERE,"HLT configuration failed! Check configuration");
	return false;
      }
    }catch(std::exception &ex){
      std::cerr<<HLTMP::getTimeTag()<<"Caught exception \""<<ex.what()<<"\" during Steering configuration"<<std::endl; 
      std::string errMsg=std::string("HLTSteering configuration failed with \"")+ex.what()+"\" Check configuration";
      ers::fatal(HLTMPPUIssue::ConfigurationIssue(ERS_HERE,errMsg.c_str()));
      throw HLTMPPUIssue::ConfigurationIssue(ERS_HERE,errMsg.c_str());
      return false;
    }
  }

  //::sleep(1);
  //ERS_INFO("Steering code loading failed continuing with dummy");  
  //return retVal;
  if(!retVal){
    throw HLTMPPUIssue::ConfigurationIssue(ERS_HERE,"Configure failed! Check Configuration");
  }
  m_motherInfo->NumKills=0;
  m_motherInfo->NumForks=0;
  m_motherInfo->LastUserCommand=std::vector<std::string>();
  m_motherInfo->LastStateTransition="Configure";
  m_motherInfo->UnexpectedChildExits=0;
  m_motherInfo->LastExitedChild="";
  m_motherInfo->NumRequested=m_numChildren;
  m_motherInfo->NumActive=0;
  m_motherInfo->NumExited=0;
  char * tmp = getenv("TDAQ_PARTITION");
  std::shared_ptr<IPCPartition> m_part(0);
  ERS_LOG("Starting IS Publishing");
  if(tmp){
    ERS_LOG("Using partition "<<tmp<<" server "<<m_ISSName<<" with object name "<<m_myName<<".PU_MotherInfo");
    try{
      m_part=std::make_shared<IPCPartition>(tmp);
    }catch(std::exception &ex){
      ERS_LOG("Can't create partition object "<<ex.what());
    }
  }
  if(m_part){
    auto id=std::make_shared<ISInfoDictionary>(*m_part);
    std::string objName=m_ISSName+"."+m_myName+".PU_MotherInfo";
    publishMotherInfo(id,objName);
  }
  ERS_DEBUG(1, "configure finished");
  if(m_saveConfigOpts){
    HLTMP::dump2File("Configure.xml",args);
    try{
      std::ofstream of("Environ.sh",std::ofstream::out|std::ofstream::trunc);
      for (char **env=environ; *env!=0;env++){
	of<<"export "<<*env<<std::endl;
      }
      of.close();
    }catch(std::exception &ex){
      std::cerr<<HLTMP::getTimeTag()<<"Failed to dump the environment to file. Error was "<<ex.what()<<std::endl;
    }
  }
  return retVal;
}

bool HLTMPPU::connect(const boost::property_tree::ptree& args) {
  ERS_LOG("pid=" << m_myPid << ", using the following configuration tree: ");
  printPtree(args,"");
  if(m_saveConfigOpts){
    HLTMP::dump2File("Connect.xml",args);
  }

  bool retVal=true;
  if(m_HLTSteering){
    retVal=(m_HLTSteering)->connect(args);
  }
  if(!retVal){
    ers::fatal(HLTMPPUIssue::TransitionIssue(ERS_HERE,"HLTSteering connect() transition failed. Check configuration"));
    throw HLTMPPUIssue::TransitionIssue(ERS_HERE,"HLTSteering connect() transition failed. Check configuration");
    return false;
  }
  ERS_DEBUG(1, "connect finished");

  return retVal;
}

bool HLTMPPU::prepareForRun(const boost::property_tree::ptree& args) {
  ERS_LOG("pid=" << m_myPid << ", using the following configuration tree: ");

  printPtree(args,"");
  if(m_saveConfigOpts){
    HLTMP::dump2File("PrepareForRun.xml",args);
  }

  m_prepareForRunTree=args;
  bool retVal=true;
  m_motherInfo->NumKills=0;
  m_motherInfo->NumForks=0;
  m_motherInfo->LastUserCommand=std::vector<std::string>();
  m_motherInfo->LastStateTransition="prepareForRun";
  m_motherInfo->UnexpectedChildExits=0;
  m_motherInfo->LastExitedChild="";
  m_motherInfo->NumRequested=m_numChildren;
  m_motherInfo->NumActive=0;
  m_motherInfo->NumExited=0;
  if(m_infoService){
    bool ret=false;
    try{
      ret=m_infoService->prepareForRun(args);
    }catch(ers::Issue &ex){
      ers::fatal(HLTMPPUIssue::TransitionIssue(ERS_HERE,"InfoService failed to complete prepareForRun",ex));
    }catch(...){
      ret=false;
    }
    if(!ret){
      ERS_LOG("InfoService prepareForRun failed");
      return false;
    }
  }

  if(m_HLTSteering){
    try{
      retVal=m_HLTSteering->prepareForRun(args);
    }catch(std::exception &ex){
      std::string errMsg=std::string("PSC threw an exception \"")+ex.what()+"\"";
      ers::fatal(HLTMPPUIssue::UnexpectedIssue(ERS_HERE,errMsg.c_str()));
      throw HLTMPPUIssue::UnexpectedIssue(ERS_HERE,errMsg.c_str());
      return false;
    }
  }
  if(!retVal){
    ers::fatal(HLTMPPUIssue::TransitionIssue(ERS_HERE,"PSC failed prepareForRun transition."));
    throw HLTMPPUIssue::TransitionIssue(ERS_HERE,"PSC failed prepareForRun transition.");
    return retVal;
  }
  if(m_nannyThread.get_id() != boost::thread::id()){
    ERS_LOG("Waiting for nanny thread to Join");
    stopNanny();
  }
  if(m_motherPublisher.get_id() != boost::thread::id()){
    ERS_LOG("Waiting for MotherInfo publisher thread to Join");
    stopMotherPublisher();
  }
  if(m_numChildren<0)m_numChildren=-m_numChildren;
  ERS_LOG("Preparing to fork "<<m_numChildren<<" child processes. Shutting down IPC."
          <<" Mother process will be outside IPC until forking is completed!.");

  if(m_childInfo){
    m_childInfo.reset(nullptr);
  }
  m_childInfo=std::make_unique<HLTMP::HLTMPPUInfo>();
  try{
    IPCCore::shutdown();
  }catch(daq::ipc::NotInitialized &ex){
    std::cerr<<"******************************   ERROR  ******************************"<<std::endl;
    std::cerr<<HLTMP::getTimeTag()<<"IPC shutdown failed with NotInitialized! reason= "<<ex.what()<<std::endl;
    std::cerr<<"******************************   ERROR  ******************************"<<std::endl;

  }catch(daq::ipc::CorbaSystemException &ex){
      std::cerr<<"******************************   ERROR  ******************************"<<std::endl;
      std::cerr<<HLTMP::getTimeTag()<<"IPC shutdown failed with CorbaSystemException! reason= "<<ex.what()<<std::endl;
      std::cerr<<"******************************   ERROR  ******************************"<<std::endl;
  }catch(std::exception &ex){
    std::cerr<<HLTMP::getTimeTag()<<"Caught unexpected exception"<<std::endl;
    std::string errMsg=std::string("Caught unexpected exception \"")+ex.what()+"\" during IPC Shutdown.!";
    ers::fatal(HLTMPPUIssue::UnexpectedIssue(ERS_HERE,errMsg.c_str()));
    throw HLTMPPUIssue::UnexpectedIssue(ERS_HERE,errMsg.c_str());
    return false;
  }
  std::random_device rgen;
  std::mt19937 rEngine(rgen());//seed with a real number possibly from /dev/urandom 
  std::uniform_int_distribution<int> uniform_dist(0,((m_forkDelay>0)?m_forkDelay:1000));
  if(m_preforkSleep>0){
    std::cout<<HLTMP::getTimeTag()<<"Sleeping for "<<m_preforkSleep<<" milliseconds before starting fork process"<<std::endl;
    try{
      boost::this_thread::sleep(boost::posix_time::milliseconds(m_preforkSleep));//sleep for given milliseconds
    }catch(boost::thread_interrupted &ex){
    }
  }
  for(int i =1;i<=m_numChildren;i++){
    if(m_forkDelay>0){
      try{
	boost::this_thread::sleep(boost::posix_time::milliseconds(uniform_dist(rEngine)));//add a random delay
      }catch(boost::thread_interrupted &ex){
      }

    }
    pid_t t=forkChildren(i);
    if(t!=0){// mother process
      motherPostForkActions(t, i);
      m_motherInfo->NumActive++;
    }else{ //forked children
      m_myPos=i;
      return doProcessLoop(args,i);
    }
  }//Forking loop
  
  char *dummv=0;
  int dummc=0;
  try{
    IPCCore::init(dummc,&dummv);
  }catch(std::exception &ex){
    std::cerr<<"******************************   ERROR  ******************************"<<std::endl;
    std::cerr<<HLTMP::getTimeTag()<<"IPC Reinitialization failed for pid="<<m_myPid<<" with "<<ex.what()<<std::endl;
    std::cerr<<"******************************   ERROR  ******************************"<<std::endl;
    std::string errMsg=std::string("IPC Initialization failed with \"")+ex.what()+"\"";
    ers::fatal(HLTMPPUIssue::UnexpectedIssue(ERS_HERE,errMsg.c_str()));
    throw HLTMPPUIssue::UnexpectedIssue(ERS_HERE,errMsg.c_str());
    return false;
  }
  ERS_DEBUG(0,"And we are back!");
  startNanny();
  if (!m_dontPublishIS) {
    startMotherPublisher();
  }
  try{
    boost::this_thread::sleep(boost::posix_time::milliseconds(1000));//sleep for given milliseconds  
  }catch(boost::thread_interrupted &ex){
  }

  ERS_DEBUG(1, "prepareForRun finished");
  if(m_motherInfo->NumActive!=(uint)m_numChildren){
    ers::warning(HLTMPPUIssue::UnexpectedIssue(ERS_HERE,"Some children exited immediately after forking!"));
    return false;
  }
  return true;
}

bool HLTMPPU::stopRun(const boost::property_tree::ptree& args) {
  ERS_LOG("pid=" << m_myPid << ", stopping the run from the mother process");
  printPtree(args,"stopRun ");
  if(m_saveConfigOpts){
   HLTMP::dump2File("StopRun.xml",args);
  }
  stopNanny();
  ERS_LOG("Starting thread to finalize children in "<<m_FinalizeTimeout<<" seconds");
  boost::thread terminator=boost::thread(&HLTMPPU::terminateChildren,this,m_FinalizeTimeout);
  collectChildExitStatus();
  m_availableSlots.clear();
  m_posPidMap.clear();
  m_myChildren.clear();
  m_exitedChildren.clear();
  m_childPidMap.clear();
  terminator.interrupt();
  terminator.join();
  bool retVal=m_HLTSteering->stopRun(args);

  m_infoService->finalize(args);
  if(!m_keepNumForks)m_numChildren=m_configTree.get("Configuration.HLTMPPUApplication.numForks",4);
  ERS_DEBUG(1, "stopRun finished. Returning  "<<(retVal?"True":"False"));
  return retVal;
}

void HLTMPPU::terminateChildren(int timeOut){
  ERS_DEBUG(1, "Terminating children, timeout: " << timeOut );
  m_terminationStarted=false;
  if(timeOut>0){
    try{
      boost::this_thread::sleep(boost::posix_time::seconds(timeOut));
    }catch(boost::thread_interrupted &ex){
      ERS_DEBUG(1, "Interrupted, this means means children exited properly, exiting");
      return;
    }
  }
  std::map<pid_t,int> pidsToKill(m_myChildren);//make a copy
  m_terminationStarted=true;
  for(auto & [pid, pos] : pidsToKill) {
    if(::kill(pid,0)==0){
      if(::kill(pid,SIGKILL)!=0){
	//char buff[200];
	//strerror_r(errno,buff,200);
	ERS_LOG("Killing process id "<<pid<<" failed with error \""<<strerror(errno)<<"\"");
      }else{
	m_motherInfo->NumKills++;
	m_motherInfo->NumActive--;
	ERS_LOG("Killed child process "<<pid);
      }
    }
  }  
}

bool HLTMPPU::disconnect(const boost::property_tree::ptree& args) {
  ERS_LOG("pid=" << m_myPid << ", using the following configuration tree: ");
  printPtree(args,"");
  return m_HLTSteering->disconnect(args);
}

bool HLTMPPU::unconfigure(const boost::property_tree::ptree& args) {
  ERS_LOG("pid=" << m_myPid << ", using the following configuration tree: ");
  printPtree(args,"");
  if(m_saveConfigOpts){
    HLTMP::dump2File("Unconfigure.xml",args);
  }
  //::sleep(0.5);
  stopMotherPublisher();
  bool retVal=m_HLTSteering->unconfigure(args);
  ERS_DEBUG(1, "unconfigure finished. Returning "<<(retVal?"True":"False"));
  return retVal;
}

bool HLTMPPU::publishStatistics(const boost::property_tree::ptree& args) {
  ERS_LOG("pid=" << m_myPid << ", using the following configuration tree: ");
  printPtree(args,"");
  return true;
}

std::string HLTMPPU::findMatchingChildName(std::string input) {
  for (auto & element : m_childPidMap) {
    auto childName = element.first;
    if (input.find(childName) != std::string::npos) {
      return childName;  // Childname is part of the input string
    }
  }

  // None of the child names matches the input string
  ERS_LOG("Cant find matching child name, returning input string: " << input);
  return input;
}

bool HLTMPPU::hltUserCommand(const boost::property_tree::ptree& args) {
  ERS_LOG("pid=" << m_myPid << ", executing hltUserCommand");
  printPtree(args,"userCommand->");
  std::string Command=args.get_child("Configuration.COMMANDNAME").data();
  std::stringstream oss;

  // FORK is called in case there is need to fork additional children, with parameter Count
  // FORK_AFTER_CRASH is called if CHIP detected a HltpuCrashIssue sent by dcm
  if(Command=="FORK" || Command=="FORK_AFTER_CRASH"){
    int count;
    std::string forkArgs = "";

    // Interrupt nanny sleep and allow it to update information of children that died
    m_nannyThread.interrupt();
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    {
      std::lock_guard<std::mutex> lck(m_nannyMutex);
      count=m_availableSlots.size();
    }
    ERS_DEBUG(2, Command << " called, " << count << " available slots");

    if (Command=="FORK") {
      count=args.get("Configuration.COUNT", 1);  // Read count from user command parameters
      forkArgs=args.get_child("Configuration.COUNT").data();
      ERS_DEBUG(2, Command << " called with parameter COUNT=" << count);
    } else if (Command=="FORK_AFTER_CRASH") {
      if (count==0) {
        ERS_LOG(Command << " called, but there are no available slots, doing nothing");
        return 0;
      }
    }
    m_motherInfo->LastUserCommand=std::vector<std::string>{Command,forkArgs};

    stopNanny();
    stopMotherPublisher();
    try{
      IPCCore::shutdown();
    }catch(daq::ipc::NotInitialized &ex){
      std::cerr<<"******************************   ERROR  ******************************"<<std::endl;
      std::cerr<<HLTMP::getTimeTag()<<"IPC shutdown failed with NotInitialized! reason= "<<ex.what()<<std::endl;
      std::cerr<<"******************************   ERROR  ******************************"<<std::endl;
    }catch(daq::ipc::CorbaSystemException &ex){
      std::cerr<<"******************************   ERROR  ******************************"<<std::endl;
      std::cerr<<HLTMP::getTimeTag()<<"IPC shutdown failed with CorbaSystemException! reason= "<<ex.what()<<std::endl;
      std::cerr<<"******************************   ERROR  ******************************"<<std::endl;
    }catch(std::exception &ex){
      std::cerr<<HLTMP::getTimeTag()<<"Caught unexpected exception"<<std::endl;
      std::string errMsg=std::string("Caught unexpected exception \"")+ex.what()+"\" during IPC Shutdown.!";
      ers::fatal(HLTMPPUIssue::UnexpectedIssue(ERS_HERE,errMsg.c_str()));
	return false;
    }

    ERS_LOG("Number of available slots: " << m_availableSlots.size());
    for(int i=0;i<count;i++){
      int pos;
      if(m_availableSlots.empty()) { // There are no free slots, increase number of children
	m_numChildren++;
	m_motherInfo->NumRequested = m_numChildren;
	pos = m_numChildren;
      } else {
	pos=m_availableSlots.front();
        m_availableSlots.pop_front();
      }
      pid_t t = forkChildren(pos);
      if (t != 0) {  //mother 
        motherPostForkActions(t, pos);
	m_motherInfo->NumActive++;
      } else {  //children
        m_myPos=i;
        return doProcessLoop(m_prepareForRunTree, pos);
      }
    }
    if(oss.str().length()>0){
      ers::warning(HLTMPPUIssue::UnexpectedIssue(ERS_HERE,(std::string("Encountered errors during forking ")+oss.str()).c_str()));
    }
    try{
      char *dummv=0;
      int dummc=0;
      IPCCore::init(dummc,&dummv);
      //sleep(60);
    }catch(std::exception &ex){
      std::cerr<<"******************************   ERROR  ******************************"<<std::endl;
      std::cerr<<HLTMP::getTimeTag()<<"IPC Reinitialization failed for pid="<<getpid()<<" with "<<ex.what()<<std::endl;
      std::cerr<<"******************************   ERROR  ******************************"<<std::endl;
      std::cerr.flush();
    }
    startNanny();
    if (!m_dontPublishIS) {
      startMotherPublisher();
    }
  }else if(Command=="KILL"){
    std::string arg=args.get_child("Configuration.CHILDNAME").data();
    ERS_LOG("Received KILL with argument: = \"" << arg << "\"");
    m_motherInfo->LastUserCommand=std::vector<std::string>{Command,arg};
    auto childName = findMatchingChildName(arg);
    ERS_LOG("Child name = \"" << childName << "\"");
    stopNanny();
    if(m_childPidMap.find(childName)!=m_childPidMap.end()){
      auto pid = m_childPidMap[childName];
      if(::kill(pid,0)==0){
	if(::kill(pid,SIGKILL)!=0){
	  //char buff[200];
	  //strerror_r(errno,buff,200);
	  ERS_LOG("Killing process id "<<pid<<" failed with error \""<<strerror(errno)<<"\"");
	}else{
	  ERS_LOG("Killed child process "<<childName<<" pid="<<pid);
	  m_motherInfo->NumKills++;
	  m_childPidMap.erase(childName);
	}
      }else{
	ERS_LOG("Child "<<childName<<" with pid "<<pid<<" don't exist!");
      }
    }else{
      ERS_LOG("Child \""<<childName<<"\" don't exist!");      
    }
    startNanny();
  }else if(Command=="KILLALL"){
    stopNanny();
    terminateChildren(0);
    collectChildExitStatus();
  }else{
    std::string arg=args.get_child("Configuration.ARG").data();
    std::cout<<HLTMP::getTimeTag()<<"Got Command \""<<Command<<"\" with arguments \""<<arg<<"\""<<std::endl;
    std::cout<<HLTMP::getTimeTag()<<"Command transfer is not implemented yet"<<std::endl;
  }
  // BOOST_FOREACH(const boost::property_tree::ptree::value_type &v,
  // 		args.get_child("Configuration")){
			
  return true;
}

bool HLTMPPU::doEventLoop() {
    ERS_LOG("This method should not been called. It should be called on HLT side!");
    return false;
}

bool HLTMPPU::doProcessLoop(const boost::property_tree::ptree& args,int childNo){
  errno=0;
  int spidRet=setpgid(0,m_myPgid);
  if(spidRet!=0){
    //char buff[200];
    //strerror_r(errno,buff,200);
    std::string errNo;
    if(errno==EACCES){
      errNo="EACCESS";
    }else if(errno==EINVAL){
      errNo="EINVAL";
    }else if(errno==EPERM){
      errNo="EPERM";
    }else if(errno==ESRCH){
      errNo="ESRCH";
    }else{
      errNo="Unexpected error";
    }
    ERS_LOG("setpgid failed with "<<spidRet<<" "<<errNo);
    ers::error(HLTMPPUIssue::UnexpectedIssue(ERS_HERE,"Can't set pgid! ZOMBIE INFESTATION RISK!"));
  }
  ERS_LOG("I am a forked child "<<childNo<<" with pid="<<m_myPid);
  boost::property_tree::ptree conf;
  conf.put("start_id",childNo);
  conf.put("stride",m_numChildren);
  conf.put("appName",m_myName);// used by the PSC
  conf.put("clientName",m_myName);
  conf.put("workerId",childNo);//used by PSC
  conf.put("numberOfWorkers",m_numChildren);// used by PSC
  if(m_infoService){
    try{
      m_infoService->prepareWorker(conf);
    }catch(ers::Issue &ex){
      ERS_LOG("InfoService prepareWorker failed");
      ers::fatal(HLTMPPUIssue::TransitionIssue(ERS_HERE,"InfoService failed to complete prepareWorker",ex));
      return false;
    }catch(std::exception &ex){
      ERS_LOG("InfoService prepareWorker failed");
      ers::fatal(HLTMPPUIssue::TransitionIssue(ERS_HERE,"InfoService failed to complete prepareWorker",ex));
      return false;
    }

  }
  ERS_DEBUG(1,"InfoService completed preparation.");
  if(m_dataSource){
    //if(!m_dataSource->prepareForRun(args)){
    try{
      ERS_DEBUG(1,"Trying prepareForRun for datasource");
      m_dataSource->prepareForRun(args);
    }catch(ers::Issue &ex){
      ERS_LOG("DataSource prepareForRun failed with"<<ex.what());
      std::string msg="DataSource failed to complete prepareForRun transition with message: ";
      msg+=ex.what();
      ers::fatal(HLTMPPUIssue::TransitionIssue(ERS_HERE,msg.c_str(),ex));
      return false;
    }catch(std::exception &ex){
      ERS_LOG("DataSource prepareForRun failed with"<<ex.what());
      std::string msg="DataSource failed to complete prepareForRun transition with message: ";
      msg+=ex.what();
      ers::fatal(HLTMPPUIssue::TransitionIssue(ERS_HERE,msg.c_str()));
      return false;
    }
    try{
      ERS_DEBUG(1,"Trying prepareWorker for datasource");
      m_dataSource->prepareWorker(conf);
    }catch(ers::Issue &ex){
      ERS_LOG("DataSource prepareWorker failed with"<<ex.what());
      std::string msg="DataSource failed to complete prepareWorker transition with message: ";
      msg+=ex.what();
      ers::fatal(HLTMPPUIssue::TransitionIssue(ERS_HERE,msg.c_str(),ex));
      return false;
    }catch(std::exception &ex){
      ERS_LOG("DataSource prepareWorker failed with"<<ex.what());
      std::string msg="DataSource failed to complete prepareWorker transition with message: ";
      msg+=ex.what();
      ers::fatal(HLTMPPUIssue::TransitionIssue(ERS_HERE,msg.c_str()));
      return false;
    }
    ERS_DEBUG(1,"DataSource completed preparation");
    if(m_HLTSteering){
      ERS_DEBUG(1,"Trying prepareWorker for PSC");
      if(!m_HLTSteering->prepareWorker(conf)){
        ERS_LOG("HLT Steering prepareWorker failed. !");
        ers::fatal(HLTMPPUIssue::TransitionIssue(ERS_HERE,"HLT Steering failed to complete prepareWorker"));
        return false;
      }
      ERS_LOG("Starting Processing Loop");
      m_childInfo->NumEvents=0;
      m_childInfo->AcceptedEvents=0;
      m_childInfo->RejectedEvents=0;
      m_childInfo->LongestWaitForL1Result=0;
      m_childInfo->LongestProcessingTime=0;
      m_childInfo->AverageAcceptTime=0.;
      m_childInfo->AverageRejectTime=0.;
      m_childInfo->AverageProcessingTime=0.;
      m_childInfo->AverageL1ResultTime=0.;
      m_childInfo->TimePercentInProcessing=0.;
      m_childInfo->TimePercentInAccept=0.;
      m_childInfo->TimePercentInReject=0.;
      m_childInfo->TimePercentInWait=0.;
      m_childInfo->TimePercentInSend=0.;

      // TODO(cyildiz): How to publish those, from DFDataSource, or not at all???
      m_publisherThread=boost::thread(&HLTMPPU::statsPublisher,this);
      ERS_LOG("Event processing starts... ");
      bool exitCode=true;  // true is clean exit
      try {
        exitCode = m_HLTSteering->doEventLoop();  // Starts event loop on Steering side
                                       // This is a blocking call, and will
                                       // end once there are no more events
      } catch(std::exception &ex) {
        std::string msg = "Finishing event processing due to doEventLoop exception: ";
        msg += ex.what();
        ers::error(HLTMPPUIssue::UnexpectedIssue(ERS_HERE, msg.c_str()));
        exitCode = false;
      }
      m_processEvents = false;

      // do finalization.
      ERS_LOG("Processing loop finished. Finalizing");
      if(m_termStagger>0){
        waitForFreeMem(std::min((int)(m_FinalizeTimeout*0.7),m_termStagger)); // TODO(cyildiz): What should happen with this?
      }

      if(!m_HLTSteering->stopRun(conf)){
        ERS_LOG("HLT stopRun failed.");
      }else{
        ERS_LOG("HLT stopRun finished ");
        if(!m_skipFinalizeWorker){
          if(!m_HLTSteering->finalizeWorker(conf)){
            ERS_LOG("HLT Finalize worker failed.");
          }
          ERS_LOG("HLTSteering finalizeWorker finished ");
        }else{
          ERS_LOG("HLTSteering finalizeWorker skipped! ");
        }
      }
      ERS_LOG("Steering finalization completed");
      delete m_HLTSteering;
      ERS_DEBUG(2, "Steering deletion completed");

      ERS_LOG("Waiting for last publication.");//need to fix this
      m_publisherThread.interrupt();
      m_publisherThread.join();
      ERS_LOG("Publisher thread joined.");
      m_dataSource->finalizeWorker(conf);
      m_dataSource->finalize(conf);
      delete m_dataSource;
      ERS_LOG("DataFlow finalization completed.");
      ERS_LOG("Shutting down InfoService.");//need to fix this
      m_infoService->finalizeWorker(conf);
      m_infoService->finalize(conf);
      delete m_infoService;
      m_infoService=0;
      m_childInfo.reset(nullptr);
      ERS_LOG("Returning from children.");
      if(m_exitImmediately){
        std::cerr<<HLTMP::getTimeTag()<<" Direct exit requested. Calling std::_Exit()"<<std::endl;
        std::cerr.flush();
        std::_Exit(120);
      }
      return exitCode;
    }else{
      ERS_LOG("No HLT Steering defined! Returning false");
      ers::fatal(HLTMPPUIssue::ConfigurationIssue(ERS_HERE,"No HLT Steering defined yet prepareForRun is called!"));
      return false;
    }
  }
  return true;
}

bool HLTMPPU::prepareWorker(const boost::property_tree::ptree& /*args*/) {
  ers::fatal(HLTMPPUIssue::UnexpectedIssue(ERS_HERE,"prepareWorker method called!"));
  return false;
}

bool HLTMPPU::finalizeWorker(const boost::property_tree::ptree& /*args*/) {
  ers::fatal(HLTMPPUIssue::UnexpectedIssue(ERS_HERE,"finalizeWorker method called!"));
  return true;
}

pid_t HLTMPPU::forkChildren(int pos){
  if(m_dumpFD)printOpenFDs("mother pre-fork->");
  auto nthreads = countThreads();
  bool threadsExist=(nthreads>0);
  ERS_DEBUG(1, nthreads << " threads exist before forking(other than main thread)");
  if(m_dumpThreads || threadsExist)printTasks("mother pre-fork Tasks(threads)->");
  ERS_DEBUG(1, "Waiting until there is " << m_allowedExtraThreadsAtFork << " or less extra threads."
            << " The wait will timeout after " << m_threadWaitTimeout << " seconds.");

  auto timeout_end = std::chrono::steady_clock::now() + std::chrono::seconds(m_threadWaitTimeout);
  while (countThreads() > m_allowedExtraThreadsAtFork) {
    std::this_thread::sleep_for(std::chrono::milliseconds(100));

    auto tnow = std::chrono::steady_clock::now();
    if (tnow > timeout_end) {
      std::string msg = "Timeout reached before number of extra threads reached "
                        "the desired value. To fix the problem it may help to increase "
                        "allowedThreadsAtFork and threadWaitTimeout in ExtraParams. ";
      msg += "Number of extra threads: " + std::to_string(countThreads());
      ERS_LOG(msg);
      ers::warning(HLTMPPUIssue::ConfigurationIssue(ERS_HERE, msg.c_str()));
      break;
    }
  }
  ERS_DEBUG(1, "Waiting done");

  pid_t t=fork();
  if(t!=0){// parent process
    m_motherInfo->NumForks++;
    if(m_dumpFD)printOpenFDs("mother post-fork->");
    if(m_dumpThreads)printTasks("mother post-fork Tasks(threads)->");
    auto appname = getAppName(pos);
    ERS_LOG("Forked child \"" << appname << "\" with pid= " << t);
    m_childPidMap[appname]=t;
    fflush(stdout);
    fflush(stderr);
    return t;
  }else{// child process detached from parent.
    if(m_dumpFD)printOpenFDs("child pre-redirection->");
    if(m_dumpThreads)printTasks("child pre-redirection Tasks(threads)->");
    char *dummv=0;
    int dummc=0;
    ERS_DEBUG(1, "Fork done");
    m_myPid=getpid();
    char * TDAQAPPNAME=getenv("TDAQ_APPLICATION_NAME");
    std::string logsPath=m_childLogPath;
    std::string logsName=m_childLogName;
    auto tnow=std::chrono::system_clock::now();
    long tepoch=std::chrono::duration_cast<std::chrono::seconds>(tnow.time_since_epoch()).count();
    if(logsPath.empty()){
      logsPath="/tmp";
    }

    // Set the new m_myName with appending position to TDAQ_APPLICATION_NAME
    m_myName = getAppName(pos);
    setenv("TDAQ_APPLICATION_NAME",m_myName.c_str(),1);

    std::stringstream ss;
    if(logsName.empty()){  // If childLogName is not specified, default is TDAQAPPNAME-PID-TIMESTAMP
      ss << m_myName << "-" << m_myPid << "-" << tepoch;
    } else { // Set it to "LOGNAMEXX"
      ss << logsName << std::setfill('0') << std::setw(2) << pos;
    }
    logsName = ss.str();


    ss.str(std::string()); // Empty stringstream
    ss << logsPath << "/" << logsName;

    auto full_logpath = ss.str();

    ERS_LOG("I am the child # "<<pos<<" with pid "<<m_myPid<<". Redirecting stdout to "<<full_logpath<<".out");
    ERS_LOG("I am the child # "<<pos<<" with pid "<<m_myPid<<". Redirecting stderr to "<<full_logpath<<".err");

    // Now do the actual redirection
    fflush(stdout);
    fflush(stderr);
    freopen((full_logpath+".out").c_str(),"w",stdout);
    freopen((full_logpath+".err").c_str(),"w",stderr);


    if(m_dumpFD)printOpenFDs("child post-redirection->");
    if(m_dumpThreads)printTasks("child post-redirection Tasks(threads)->");
    try{
      IPCCore::init(dummc,&dummv);
      ERS_LOG("******************************  IPC INIT SUCCEEDED  ******************************");
    }catch(std::exception &ex){
      std::cerr<<"******************************   ERROR  ******************************"<<std::endl;
      std::cerr<<HLTMP::getTimeTag()<<"IPC Reinitialization failed for pid="<<getpid()<<" with "<<ex.what()<<std::endl;
      std::cerr<<"******************************   ERROR  ******************************"<<std::endl;
      std::cerr.flush();
    }

    // Ensure that child processes will exit if mother dies/exits.
    prctl( PR_SET_PDEATHSIG, SIGTERM );
    ::signal(SIGCHLD,SIG_DFL);
    ::signal(SIGKILL,SIG_DFL);
    ::signal(SIGTERM,SIG_DFL);

    ERS_LOG("I am the child # "<<pos<<" with, pid="<<m_myPid<<". Redirection is completed");
    ss.str(std::string());
    ss << TDAQAPPNAME << "-" << std::setfill('0') << std::setw(2) << pos;
    m_myName=ss.str();
  }
  return t;
}

void HLTMPPU::doNannyWork() {
  int count = 0;
  pid_t childpid = 0;
  while (m_nannyWork && childpid!=-1) {
    errno = 0;
    int retVal = 0;
    childpid = waitpid(0, &retVal, WNOHANG);
    if (childpid==0) {  // if all children working sleep 1 seconds
      try {
	boost::this_thread::sleep(boost::posix_time::seconds(1));
      } catch (boost::thread_interrupted &ex) {  //don't need to sleep anymore
	if (!m_nannyWork) return;
      }
    } else if (childpid==-1) {  // if no child remains
      m_motherInfo->NumActive = 0;
      if (errno==ECHILD) {
	ERS_LOG("All children have exited. Returning");
	return;
      }
      count++;
      ERS_LOG("waitpid returned " << ::strerror(errno));
      if (count>10) {
	ERS_LOG("Returning because of killswitch");
	return;
      }
    } else {  // something happened to children with pid=childpid
      std::lock_guard<std::mutex> lck(m_nannyMutex);
      if (m_myChildren.find(childpid)==m_myChildren.end()) {
        ERS_LOG("Something happened to child process with pid: " << childpid
                << " but it wasn't one of the forks. This may happen "
                << " in standalone running with runHLTMPPy or athenaHLT.");
        continue;
      }
      if (WIFEXITED(retVal)) {  // Means child terminated normally
	int exitStat = WEXITSTATUS(retVal);
	ERS_LOG("Child with PID=" << childpid << " exited with status=" << exitStat);
	if (exitStat) {
	  std::stringstream ss;
          ss << "Child pid=" << childpid << " exited with unexpected return value " << exitStat;
	  ers::warning(HLTMPPUIssue::ChildIssue(ERS_HERE, ss.str().c_str()));
	  m_motherInfo->UnexpectedChildExits++;
	}
        updateExitedChildren(childpid);
      }
      if (WIFSIGNALED(retVal)) {
	int exitStat = WTERMSIG(retVal);
	ERS_LOG("Child with PID=" << childpid << " exited with a signal=" << exitStat);
	if (exitStat) {
	  m_motherInfo->UnexpectedChildExits++;
	  std::stringstream ss;
          ss << "Child pid=" << childpid << " exited with signal " << exitStat;
	  ers::warning(HLTMPPUIssue::ChildIssue(ERS_HERE, ss.str().c_str()));
	}
        updateExitedChildren(childpid);
      }
      if (WIFSTOPPED(retVal)) {
        int exitStat = WSTOPSIG(retVal);
	ERS_LOG("Child with PID=" << childpid << " stopped by a signal=" << exitStat);
	std::stringstream ss;
        ss << "Child pid=" << childpid << " stopped with signal " << exitStat;
	ers::warning(HLTMPPUIssue::ChildIssue(ERS_HERE, ss.str().c_str()));
	if (m_myChildren.find(childpid) != m_myChildren.end()) {
	  m_availableSlots.push_back(m_myChildren[childpid]);
          m_myChildren.erase(childpid);
        }
      }
    }
  }
}

void HLTMPPU::collectChildExitStatus(){

  std::stringstream oss;
  for (auto &[pid,slot] : m_myChildren) {
    oss << pid << " ";
  }
  ERS_LOG("current children list: " << oss.str());

  int count=0;
  pid_t childpid=0;
  while (childpid!=-1) {
    errno = 0;
    int retVal = 0;
    childpid = waitpid(0, &retVal, WNOHANG);
    if (childpid==0) {  // if all children working sleep 1 seconds
      if (m_myChildren.empty()) {
        ERS_LOG("There are child processes that are alive, but all forks seem to have exited. "
                << "This may happen in standalone running with runHLTMPPy or athenaHLT.");
        // runHLTMPPy.py scripy may start infrastructure such as ipc_server, is_server, rdb_server
        // All these processes are also child processes and affect the result of waitpid
        // This should not affect online running
        return;
      }
    } else if (childpid==-1) {  // if no child remains
      if (errno==ECHILD) {
	ERS_LOG("All children have exited. Returning");
	return;
      }
      count++;
      ERS_LOG("waitpid returned " << ::strerror(errno));
      if (count>10) {
	ERS_LOG("Returning because of killswitch");
	return;
      }
    } else {  // something happened to children
      if (WIFEXITED(retVal)) {
	int exitStat = WEXITSTATUS(retVal);
	ERS_LOG("Child pid=" << childpid << " exited with status=" << exitStat);
	if (exitStat && !m_terminationStarted) {
	  std::stringstream ss;
          ss << "Child pid=" << childpid << " exited with unexpected return value " << exitStat;
	  ers::warning(HLTMPPUIssue::ChildIssue(ERS_HERE, ss.str().c_str()));
	}
	if (m_myChildren.find(childpid) != m_myChildren.end()) {
	  m_availableSlots.push_back(m_myChildren[childpid]);
	  m_myChildren.erase(childpid);
	}
      }
      if (WIFSIGNALED(retVal)) {
	int exitStat = WTERMSIG(retVal);
	ERS_LOG("Child pid=" << childpid << " exited with signal=" << exitStat);
	if (exitStat && !m_terminationStarted) {
	  std::stringstream ss;
          ss << "Child pid=" << childpid << " exited with signal " << exitStat;
	  ers::warning(HLTMPPUIssue::ChildIssue(ERS_HERE, ss.str().c_str()));
	}
	if (m_myChildren.find(childpid) != m_myChildren.end()) {
	  m_availableSlots.push_back(m_myChildren[childpid]);
	  m_myChildren.erase(childpid);
	}
      }
      if (WIFSTOPPED(retVal)) {
        int exitStat = WSTOPSIG(retVal);
	std::stringstream ss;
        ss << "Child pid=" << childpid << " stopped with signal " << exitStat;
	ERS_LOG(ss.str().c_str());
	ers::warning(HLTMPPUIssue::ChildIssue(ERS_HERE, ss.str().c_str()));
	if (m_myChildren.find(childpid) != m_myChildren.end()) {
	  m_availableSlots.push_back(m_myChildren[childpid]);
          m_myChildren.erase(childpid);
        }
      }
    }
  }
}

void HLTMPPU::printPtree(const boost::property_tree::ptree& args, std::string level){
  level+=" ";
  BOOST_FOREACH(const boost::property_tree::ptree::value_type &v, args) {
    std::string val(v.second.get_value<std::string>());
    boost::algorithm::trim(val);
    std::cout<<level<<v.first<<" : "<<val<<std::endl;
    printPtree(v.second,level);
  }
}

void HLTMPPU::startNanny(){
  if(m_nannyThread.get_id() != boost::thread::id())return;
  m_nannyWork=true;
  ERS_LOG("Starting nanny thread in mother process");
  m_nannyThread=boost::thread(&HLTMPPU::doNannyWork,this);
}

void HLTMPPU::stopNanny(){
  m_nannyWork=false;
  if(m_nannyThread.get_id() == boost::thread::id())return;
  ERS_LOG("Stopping nanny thread in mother process");
  m_nannyThread.interrupt();
  m_nannyThread.join();
}

void HLTMPPU::getAndCalculateChildStatistics() {
  std::lock_guard<std::mutex> lck(m_statMutex);
  boost::property_tree::ptree stats = m_dataSource->getStatistics();
  ERS_DEBUG(5,"Start");

  m_childInfo->NumEvents=stats.get<uint64_t>("numEventsTotal",0);
  m_childInfo->AcceptedEvents=stats.get<uint64_t>("acceptedEventsTotal",0);
  m_childInfo->RejectedEvents=stats.get<uint64_t>("rejectedEventsTotal",0);

  m_childInfo->LongestWaitForL1Result=stats.get<uint64_t>("longestWaitForL1Result",0);
  m_childInfo->LongestProcessingTime=stats.get<uint64_t>("longestProcessingTime",0);

  auto eventsInInterval = stats.get<uint64_t>("numEvents",0);
  auto acceptedInInterval = stats.get<uint32_t>("acceptedEvents",0);
  auto rejectedInInterval = stats.get<uint64_t>("rejectedEvents",0);
  if(eventsInInterval){
    double ievts=1.0/eventsInInterval;
    m_childInfo->AverageL1ResultTime=stats.get<double>("waitDuration",0.)*ievts;
    m_childInfo->AverageProcessingTime=stats.get<double>("processDuration",0.)*ievts;
  }
  if(acceptedInInterval){
    m_childInfo->AverageAcceptTime=stats.get<double>("acceptDuration",0.)/acceptedInInterval;
  }
  if(rejectedInInterval){
    m_childInfo->AverageRejectTime=stats.get<double>("rejectDuration",0.)/rejectedInInterval;
  }

  auto totDuration = stats.get<double>("totalDuration",0.);
  if(totDuration){
    double invDur=1./totDuration;
    m_childInfo->TimePercentInProcessing=stats.get<double>("processDuration",0.)*invDur;
    m_childInfo->TimePercentInAccept=stats.get<double>("acceptDuration",0.)*invDur;
    m_childInfo->TimePercentInReject=stats.get<double>("rejectDuration",0.)*invDur;
    m_childInfo->TimePercentInWait=stats.get<double>("waitDuration",0.)*invDur;
    m_childInfo->TimePercentInSend=stats.get<double>("sendDuration",0.)*invDur;
  }

  ERS_DEBUG(5,"End");
}

void HLTMPPU::statsPublisher(){
  char * tmp = getenv("TDAQ_PARTITION");
  std::shared_ptr<IPCPartition> m_part(0);
  ERS_LOG("Starting IS Publishing");
  if(tmp){
    ERS_LOG("Using partition "<<tmp<<" server "<<m_ISSName<<" with object name "<<m_myName<<".PU_ChildInfo");
    try{
      m_part=std::make_shared<IPCPartition>(tmp);
    }catch(std::exception &ex){
      ERS_LOG("Can't create partition object "<<ex.what());
    }
  }
  if(m_part){
    ISInfoDictionary id(*m_part);
    std::string objName=m_ISSName+"."+m_myName+".PU_ChildInfo";
    boost::chrono::steady_clock::time_point now=boost::chrono::steady_clock::now();
    boost::chrono::seconds timeFromEpoch=
      boost::chrono::duration_cast<boost::chrono::seconds>(now.time_since_epoch());    
    auto toNext=m_publishInterval-(timeFromEpoch.count()%m_publishInterval);
    auto sleepDuration=boost::chrono::seconds(m_publishInterval);
    auto pubTime=now+boost::chrono::seconds(toNext);
    try{
      boost::this_thread::sleep_until(pubTime);
    }catch(boost::thread_interrupted &ex){
      ERS_LOG("Publisher thread sleep is interrupted before first checkin");
    }
    try{
      getAndCalculateChildStatistics();
      if (!m_dontPublishIS) {
        id.checkin(objName,*m_childInfo);
        ERS_DEBUG(2, "First IS checkin succesful");
      }
    }catch(daq::is::Exception &ex){
      ERS_LOG("Caught exception "<<ex.what()<<" in first check-in");
    }
    pubTime+=sleepDuration;
    while(m_processEvents){
      try{
	boost::this_thread::sleep_until(pubTime);
	pubTime+=sleepDuration;
      }catch(boost::thread_interrupted &ex){
	ERS_LOG("Publisher thread sleep is interrupted");
	auto nextPoint=
	  boost::chrono::duration_cast<boost::chrono::milliseconds>(boost::chrono::steady_clock::now().time_since_epoch());    
	auto toNext=m_publishInterval*1000-(nextPoint.count()%(m_publishInterval*1000));
	pubTime+=boost::chrono::milliseconds(toNext);
      }
      try{
        getAndCalculateChildStatistics();
        if (!m_dontPublishIS) {
          id.checkin(objName,*m_childInfo);
          ERS_DEBUG(3,"IS checkin done");
        }
      }catch(daq::is::Exception &ex){
	ERS_LOG("Caught exception "<<ex.what()<<" while stats publication");
      }
    }
  }else{
    ERS_LOG("Can't get partition object");
  }
}

void HLTMPPU::printOpenFDs(const std::string &header=""){
  DIR *dir;
  struct dirent *ent;
  pid_t mypid=getpid();
  if ((dir = opendir ("/proc/self/fd/")) != NULL) {
    /* print all the files and directories within directory */
    ERS_LOG(header<<" "<<"List of open FDs (one will be due to this call) pid="<<getpid());
    while ((ent = readdir (dir)) != NULL) {
      std::string name(ent->d_name);
      if(name=="."||name=="..") continue;
      std::string typ;
      if(ent->d_type==DT_BLK){
	typ="BLOCK";
      }else if(ent->d_type==DT_CHR){
	typ="CHARACTER";
      }else if(ent->d_type==DT_DIR){
	typ="DIR";
      }else if(ent->d_type==DT_FIFO){
	typ="FIFO";
      }else if(ent->d_type==DT_LNK){
	typ="LINK";
	char buf[2001];
	std::string path="/proc/self/fd/"+name;
	int len=readlink(path.c_str(),buf,2000);
	if(len>0){
	  buf[len]='\0';
	  typ+=" -> "+std::string(buf);
	}
      }else if(ent->d_type==DT_REG){
	typ="FILE";
      }else if(ent->d_type==DT_SOCK){
	typ="SOCKET";
      }else if(ent->d_type==DT_UNKNOWN){
	typ="UNKNOWN";
      }
      ERS_LOG(header<<" "<<mypid<<" "<<name<<" type="<<typ);
    }
    closedir (dir);
  } else {
    /* could not open directory */
    perror ("");
    std::cerr<<"Can't open /proc/self/fd"<<std::endl;
  }
}

void HLTMPPU::printTasks(const std::string &header=""){
  DIR *dir;
  struct dirent *ent;
  pid_t mypid=getpid();
  if ((dir = opendir ("/proc/self/task/")) != NULL) {
    /* print all the files and directories within directory */
    ERS_LOG(header<<" "<<"List of open FDs (one will be due to this call) pid="<<getpid());
    while ((ent = readdir (dir)) != NULL) {
      std::string name(ent->d_name);
      if(name=="."||name=="..") continue;
      std::string typ;
      if(ent->d_type==DT_BLK){
	typ="BLOCK";
      }else if(ent->d_type==DT_CHR){
	typ="CHARACTER";
      }else if(ent->d_type==DT_DIR){
	typ="DIR";
      }else if(ent->d_type==DT_FIFO){
	typ="FIFO";
      }else if(ent->d_type==DT_LNK){
	typ="LINK";
	char buf[2001];
	std::string path="/proc/self/task/"+name;
	int len=readlink(path.c_str(),buf,2000);
	if(len>0){
	  buf[len]='\0';
	  typ+=" -> "+std::string(buf);
	}
      }else if(ent->d_type==DT_REG){
	typ="FILE";
      }else if(ent->d_type==DT_SOCK){
	typ="SOCKET";
      }else if(ent->d_type==DT_UNKNOWN){
	typ="UNKNOWN";
      }
      ERS_LOG(header<<" "<<mypid<<" "<<" threadPID= "<<name<<" type="<<typ);
    }
    closedir (dir);
  } else {
    /* could not open directory */
    perror ("");
    std::cerr<<"Can't open /proc/self/task"<<std::endl;
  }
}
 
 unsigned int HLTMPPU::countThreads(){
   DIR *dir;
   struct dirent *ent;
   int nThreads=0;
   pid_t mypid=getpid();
   if ((dir = opendir ("/proc/self/task/")) != NULL) {
     while ((ent = readdir (dir)) != NULL) {
       std::string name(ent->d_name);
       if(name=="."||name=="..") continue;
       std::string typ;
       if(ent->d_type==DT_BLK){
	 typ="BLOCK";
       }else if(ent->d_type==DT_CHR){
	 typ="CHARACTER";
       }else if(ent->d_type==DT_DIR){
	 typ="DIR";
       }else if(ent->d_type==DT_FIFO){
	 typ="FIFO";
       }else if(ent->d_type==DT_LNK){
	 typ="LINK";
	 char buf[2001];
	 std::string path="/proc/self/task/"+name;
	 int len=readlink(path.c_str(),buf,2000);
	 if(len>0){
	   buf[len]='\0';
	   typ+=" -> "+std::string(buf);
	 }
       }else if(ent->d_type==DT_REG){
	 typ="FILE";
       }else if(ent->d_type==DT_SOCK){
	 typ="SOCKET";
       }else if(ent->d_type==DT_UNKNOWN){
	 typ="UNKNOWN";
       }
       int currPid=0;
       try{
	 currPid=std::stoi(name);
       }catch(std::exception &ex){
       }
       if(mypid!=currPid){
	 nThreads++;
       }
     }
     closedir (dir);
     return nThreads;
   } else {
     /* could not open directory */
     perror ("");
     std::cerr<<"Can't open /proc/self/task"<<std::endl;
     return -1;
   }
   return nThreads;
 }

void HLTMPPU::publishMotherInfo(std::shared_ptr<ISInfoDictionary> dict,const std::string& name){
  try{
    dict->checkin(name,*m_motherInfo);
    m_lastPublish=boost::chrono::steady_clock::now();
  }catch(daq::is::Exception &ex){
    ERS_LOG("Caught exception "<<ex.what()<<" while Object deletion and creation");
  }
}

void HLTMPPU::startMotherPublisher(){
  if(m_motherPublisher.get_id() != boost::thread::id())return;
  m_publisherWork=true;
  ERS_DEBUG(1, "Starting MotherInfo publisher thread");
  m_motherPublisher=boost::thread(&HLTMPPU::doMotherPublication,this);
}

void HLTMPPU::stopMotherPublisher(){
  m_publisherWork=false;
  if(m_motherPublisher.get_id() == boost::thread::id())return;
  ERS_DEBUG(1, "Stopping MotherInfo publisher thread");
  m_motherPublisher.interrupt();
  m_motherPublisher.join();
  std::this_thread::sleep_for(std::chrono::milliseconds(500));// wait for potential ipc operations
}

void HLTMPPU::doMotherPublication(){
  char * tmp = getenv("TDAQ_PARTITION");
  std::shared_ptr<IPCPartition> part;
  std::shared_ptr<ISInfoDictionary> id;
  std::string objName=m_ISSName+"."+m_myName+".PU_MotherInfo";

  if(tmp){
    ERS_LOG("Using partition "<<tmp<<" server "<<m_ISSName<<" with object name "<<m_myName<<".PU_MotherInfo");
    try{
      part=std::make_shared<IPCPartition>(tmp);
    }catch(std::exception &ex){
      ERS_LOG("Can't create partition object "<<ex.what());
    }
  }
  if(part){
    id=std::make_shared<ISInfoDictionary>(*part);
  }

  boost::chrono::steady_clock::time_point now=boost::chrono::steady_clock::now();
  boost::chrono::seconds timeFromEpoch=boost::chrono::duration_cast<boost::chrono::seconds>(now.time_since_epoch());
  boost::chrono::seconds timeFromLast=timeFromEpoch-boost::chrono::duration_cast<boost::chrono::seconds>(m_lastPublish.time_since_epoch());

  auto toNext=m_publishInterval-(timeFromEpoch.count()%m_publishInterval);
  if((timeFromLast.count()>m_publishInterval)&&(toNext>m_publishInterval*0.5)){// we missed some publications
    publishMotherInfo(id,objName);
  }
  int64_t numPublishes=timeFromEpoch.count()/m_publishInterval+1;
  boost::chrono::steady_clock::time_point nextPublish(boost::chrono::seconds(numPublishes*m_publishInterval));
  while(m_publisherWork){
    try{
      boost::this_thread::sleep_until(nextPublish);
    }catch(boost::thread_interrupted &ex){
      //don't need to sleep anymore
      if(!m_publisherWork) return;
    }
    publishMotherInfo(id,objName);
    nextPublish+=boost::chrono::seconds(m_publishInterval);
  }

}

void HLTMPPU::waitForFreeMem(int maxSleep){
  if(m_myPos==0)return;
  if(maxSleep<=0)return;
  auto twait=std::chrono::steady_clock::now()+std::chrono::seconds(maxSleep);
  char buff[1000];
  std::ifstream selfMem("/proc/self/statm/");
  std::string line;
  std::getline(selfMem,line);
  long pageSize=sysconf(_SC_PAGESIZE);
  if(pageSize<1){
    ERS_LOG("Couldn't get page size. Errno was ="<<errno<< ". Assuming Pagesize= 4096. ");
    errno=0;
    pageSize=4096;
  }
  unsigned int vmTot,vmRss,vmShare,vmText,vmLib,vmData,vmDirty;
  
  sscanf(line.c_str(),"%u %u %u %u %u %u %u",&vmTot,&vmRss,&vmShare,&vmText,&vmLib,&vmData,&vmDirty);
  selfMem.close();
  vmTot*=pageSize;
  vmRss*=pageSize;
  std::set<int> activeSiblings;

  while(std::chrono::steady_clock::now()<twait){
    try{
      std::ifstream meminfo("/proc/meminfo");
      std::getline(meminfo,line);//MemTotal
      unsigned int memTotal=0,memFree=0,memAvail=0;
      sscanf(line.c_str(),"%s: %u kB",buff,&memTotal);
      std::getline(meminfo,line);//MemTotal
      sscanf(line.c_str(),"%s: %u kB",buff,&memFree);
      std::getline(meminfo,line);//MemTotal
      sscanf(line.c_str(),"%s: %u kB",buff,&memAvail);
      meminfo.close();
      for (auto &[pos,pid] : m_posPidMap) { // check siblings
	if(kill(pid,0)==-1){// process is missing
	  errno=0;
	  if(activeSiblings.find(pos)!=activeSiblings.end()){//process was not there before either
	    activeSiblings.erase(activeSiblings.find(pos));//take it out
	  }
	}else{// process is active
	  activeSiblings.insert(pos);
	}
      }
      if(activeSiblings.size()==0)break;
      if(memAvail-(vmRss*activeSiblings.size())>vmRss){//there is enough space for at least one more process
	break;
      }
      std::this_thread::sleep_for(std::chrono::milliseconds(1000));// wait for other processes
    }catch(std::exception &ex){
      ERS_LOG("Failed reading proc memory information. "<<ex.what());
      break;
    }
  }
}

void HLTMPPU::motherPostForkActions(pid_t pid, int pos) {
  int spidRet=setpgid(pid, m_myPgid);
  if(spidRet!=0){
    std::string errNo;
    if(errno==EACCES){
      errNo="EACCESS";
    }else if(errno==EINVAL){
      errNo="EINVAL";
    }else if(errno==EPERM){
      errNo="EPERM";
    }else if(errno==ESRCH){
      errNo="ESRCH";
    }else{
      errNo="Unexpected error";
    }
    std::stringstream oss;
    oss << HLTMP::getTimeTag() << "Can't set pgid for child " << pos << " pid=" << pid
       << " setpgid failed with " << spidRet << " errno " << errNo << std::endl;
    ers::warning(HLTMPPUIssue::UnexpectedIssue(ERS_HERE, oss.str().c_str()));
  }
  m_myChildren[pid]=pos;
  m_posPidMap[pos]=pid;
}

std::string HLTMPPU::getAppName(int position) {
  // This should be either called in mother process or before m_myName is reset in child process
  std::stringstream ss;
  ss << getenv("TDAQ_APPLICATION_NAME") << "-" << std::setfill('0') << std::setw(2) << position;
  return ss.str();
}

void HLTMPPU::updateExitedChildren(pid_t childpid) {
  m_motherInfo->NumActive--;
  m_motherInfo->NumExited++;
  if (m_myChildren.find(childpid) != m_myChildren.end()) {
    m_availableSlots.push_back(m_myChildren[childpid]);
    m_motherInfo->LastExitedChild = getAppName(m_myChildren[childpid]);
    m_myChildren.erase(childpid);
  }
}

unsigned HLTMPPU::numberOfActiveChildren() {
  return m_motherInfo->NumActive;
}
